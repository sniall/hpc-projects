case=1
if [ $case -eq 1 ] 
then
  graph='/cluster/scratch/herdem/soc-LiveJournal1/soc-LiveJournal1.mtx'
elif [ $case -eq 2 ] 
then
  graph='/cluster/scratch/herdem/com-Friendster/com-Friendster.mtx'
else
  graph='/cluster/scratch/herdem/com-Orkut/com-Orkut.mtx'
fi

model='XeonGold_5118'

export I_MPI_STATS=4,ipm

mkdir data_5118
cd data_5118
for nodes in $(seq 1 2)
do
  if [ $nodes -eq 1 ]
  then 
    mem='2GB'
  else
    mem='1GB'
  fi
  let reserve=nodes*24
  let reserve_low=reserve-23
  for ranks in $(seq $reserve_low $reserve)
  do
    mkdir $ranks
    cd $ranks
    bsub -We 01:00 -n $reserve -R "span[ptile=24]" -R fullnode -R "rusage[mem=$mem]" -R "select[model==$model]" -o "output.txt" mpirun -n $ranks ../../../pagerank_parallel $graph
    cd ..
  done
done
cd ..

model='XeonE3_1585Lv5'
module purge
module load interconnect/ethernet
module load new intel/2018.1

mkdir data_1585
cd data_1585
for nodes in $(seq 1 16)
do
  if [ $case -eq 2 ]
  then
    if [ $nodes -gt 10 ]
    then 
      mem='1GB'
    elif [ $nodes -ge 5 ]
    then 
      mem='2GB'
    elif [ $nodes -ge 2 ]
    then
      mem='5GB'
    else 
      mem='10GB'
    fi
  else
    mem='1GB'
  fi
  let reserve=nodes*4
  let reserve_low=reserve-3
  for ranks in $(seq $reserve_low $reserve)
  do
    mkdir $ranks
    cd $ranks
    bsub -We 01:00 -n $reserve -R "span[ptile=4] select[maxslots==4]" -R "rusage[mem=$mem]" -R "select[model==$model]" -o "output.txt" mpirun -n $ranks ../../../pagerank_parallel $graph
    cd ..
  done
done
cd ..

