### parse_mpi_stats.py
### Usage: python3 parse_mpi_stats.py input_directory output_file
### Make sure every subdirectory of input_directory has file stats.ipm!

import os
import sys
import re

# Read input path
input_path = sys.argv[1]

# Get ranks from subdirectories of input_directoy
ranks = sorted([ int(p) for p in os.listdir(input_path) if p != "1" ])

# Open output file
output_filename = sys.argv[2]
output_file = open(output_filename, mode='w+')

# Output file header
print("p,allreduce,allgatherv", file=output_file)

for p in ranks:
    input_filename = input_path + "/" + str(p) + "/stats.ipm"
    input_file = open(input_filename)

    allreduce_time = ""
    allgatherv_time = ""

    found_pagerank = False
    for line in input_file:
        # Search for PageRank region
        if (re.match("# region  : PageRank", line) != None):
            found_pagerank = True
        
        # Only consider timings in PageRank region
        if (found_pagerank):
            allreduce = re.match('# MPI_Allreduce( )*(\d+\.?\d*)( )*(\d+\.?\d*)( )*(\d+\.?\d*)( )*(\d+\.?\d*)', line)
            allgatherv = re.match('# MPI_Allgatherv( )*(\d+\.?\d*)( )*(\d+\.?\d*)( )*(\d+\.?\d*)( )*(\d+\.?\d*)', line)
            
            if (allreduce != None):
                allreduce_time = allreduce.group(8)
            
            elif (allgatherv != None):
                allgatherv_time = allgatherv.group(8)
            
            # If both timings found, break loop
            if (allreduce_time != "" and allgatherv_time != ""):
                break
    
    # Output timings to output file
    print(str(p) + "," + allreduce_time + "," + allgatherv_time, file=output_file)