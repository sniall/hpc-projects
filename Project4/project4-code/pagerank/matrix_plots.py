import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import scipy.io

matrix = scipy.io.mmread(sys.argv[1])
fig = plt.figure(figsize=(5, 5))
fig.add_axes()
ax = fig.add_subplot(111)
ax.spy(matrix, markersize=1)

plt.savefig(sys.argv[2], bbox_inches='tight')
