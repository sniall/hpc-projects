#!/bin/bash
# Automatically clones and configures liblsb (https://github.com/spcl/liblsb)
# Usage: ./configure_liblsb.sh directory

LIBLSB_REPO=https://github.com/spcl/liblsb
LIBLSB_PATH=$1

echo "----------------------- GET AND CONFIGURE LIBLSB ----------------------"
echo "------------------------------ clone repo -----------------------------"
git clone $LIBLSB_REPO $LIBLSB_PATH
cd $LIBLSB_PATH
echo "------------------------------ configure ------------------------------"
./configure
echo "-------------------------------- build --------------------------------"
module load new gcc/6.3.0
make
echo "--------------- FINISHED GETTING AND CONFIGURING LIBLSB ---------------"