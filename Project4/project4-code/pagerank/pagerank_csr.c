#include <assert.h>
#ifdef BENCHMARK
#include <liblsb.h>
#endif
#include <math.h>
#include <mkl.h>
#include <mkl_spblas.h>
#include <mpi.h>
#ifdef HYBRID
#include <omp.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mmio.h"

#ifdef BENCHMARK
int run_id;
#endif

/**
 * @brief Computes the PageRank vector of the connectivity matrix in parallel
 * @param[in] G Sparse connectivity matrix in CSR format of the network of size
 * nodes_per_rank x nodes_per_rank and nnz(matrix) = nedges_per_rank
 * @param[in] indices Corresponding row indices
 * @param[in] rowptr Corresponding row pointer
 * @param[in] c Vector storing column sums of matrix; length nodes
 * @param[in] nodes Global number of nodes
 * @param[in] nedges Global number of edges
 * @param[in] nodes_send Array where nodes_send[i] contains how many nodes rank
 * i owns
 * @param[in] nodes_displs Array containing displacements
 * @param[in] p Probability that the random walk follows link
 * @param[in] err Termination error for power method
 * @param[out] x PageRank vector of size nodes_per_rank; should be assembled by
 * caller
 * @param[in] comm MPI_Comm to work in
 * @pre MPI is initialized; data is distributed
 */
void PageRank(double *G, int *indices, int *rowptr, int *c, int nodes,
              int nedges, int *nodes_send, int *nodes_displs, const double p,
              double err, double *x, MPI_Comm comm) {
  int i, j, rank, size, it = 0, err_code;
  double norm = 0., norm_sq = 0., time_start;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &size);
  
  int nodes_per_rank = nodes_send[rank];
  
  double *z = (double *)malloc(nodes_per_rank * sizeof(double));
  double *xs = (double *)malloc(nodes * sizeof(double));
  double *ones = (double *)malloc(nodes_per_rank * sizeof(double));
  for (i = 0; i < nodes_per_rank; ++i) {
    ones[i] = 1.;
  }
  
  // build scaled nonzero adjacency matrix G = p*G*D
  for (i = 0; i < nodes_per_rank; i++) {
    for (j = rowptr[i]; j < rowptr[i + 1]; j++) {
      G[j] = p * G[j] / c[indices[j]];
    }
  }
  
  sparse_matrix_t G_sparse;
  mkl_sparse_d_create_csr(&G_sparse, SPARSE_INDEX_BASE_ZERO, nodes_per_rank,
                          nodes, rowptr, rowptr + 1, indices, G);
  
  struct matrix_descr descr;
  descr.type = SPARSE_MATRIX_TYPE_GENERAL;
  
  // This is to normalize the array of non zeros
  for (i = 0; i < nodes_per_rank; i++) {
    if (c[i + nodes_displs[rank]] != 0)
      z[i] = (1 - p) / (double)nodes;
    else
      z[i] = 1.0 / (double)nodes;
  }
  
  // TODO: speed up multinode/thread
  // inital guess for x (similar as in the Matlab code
  for (i = 0; i < nodes; i++) xs[i] = 1.0 / ((double)nodes);
  
  // ************************************************
  // ********* Computing PageRank *******************
  // ************************************************
#ifdef BENCHMARK
  MPI_Barrier(MPI_COMM_WORLD);  // synchronize timing
  LSB_Res();
#endif
  MPI_Pcontrol(1, "PageRank");
  
  do {
    // compute x = p*G*D*x
    mkl_sparse_d_mv(SPARSE_OPERATION_NON_TRANSPOSE, 1., G_sparse, descr, xs, 0.,
                    x);
    
    // x = p*G*D*x + e*(z*x)
    norm = cblas_ddot(nodes_per_rank, z, 1, xs + nodes_displs[rank], 1);
    
    err_code = MPI_Allreduce(MPI_IN_PLACE, &norm, 1, MPI_DOUBLE, MPI_SUM, comm);
    assert(!err_code);
    
    // x = p*G*D*x + e*(z*x)
    cblas_daxpy(nodes_per_rank, norm, ones, 1, x, 1);
    
    cblas_daxpy(nodes_per_rank, -1., x, 1, xs + nodes_displs[rank], 1);
    norm_sq = cblas_ddot(nodes_per_rank, xs + nodes_displs[rank], 1,
                         xs + nodes_displs[rank], 1);
    
    err_code =
    MPI_Allreduce(MPI_IN_PLACE, &norm_sq, 1, MPI_DOUBLE, MPI_SUM, comm);
    assert(!err_code);
    
    norm = sqrt(norm_sq);
    
    if (norm > err) {
      err_code = MPI_Allgatherv(x, nodes_per_rank, MPI_DOUBLE, xs, nodes_send,
                                nodes_displs, MPI_DOUBLE, comm);
      assert(!err_code);
    }
    it += 1;
  } while (norm > err);
  
  if (rank == 0) {
    printf("Norm: %e, iterations: %d\n", norm, it);
  }
  
#ifdef BENCHMARK
  LSB_Rec(run_id);
#endif
  MPI_Pcontrol(-1, "PageRank");
  
  free(z);
  free(xs);
  free(ones);
}

void convert_matrix(int, int, double *, int *, int *, int *);

int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);
  
  const unsigned int num_runs = 25;
  
  MPI_Pcontrol(1, "InitialDistribution");
  int size, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int err_code = 0;
  
#ifdef BENCHMARK
  LSB_Init("PageRank", 0);
  LSB_Set_Rparam_int("MPI_ranks", size);
#endif
  
  FILE *fp;
  char *filename;
  MM_typecode matcode;
  int ret_code;
  int colindex, link, i, j = 0, col, colmatch = 0, localc = 0;
  int k;
  
  int nodes;   // number of webpages = number of nodes in the graph
  int nedges;  // number of links in the webgraph = number of nonzeros in the
  // matrix
  double p = 0.85;  // probability that a user follows an outgoing link
  
  // use G as the scaled nonzero adjacency matrix stored in compress column
  // format
  double *G = NULL;
  int *indices = NULL, *colptr = NULL;
  int *rowptr = NULL;
  
  double *x = NULL;  // new solution vector x
  int *c = NULL;
  
  double err = 1.e-6;
  double norm = 0.0, norm_sq = 0.0, time_start = 0.0;
  char ch;
  char line[21];
  
  if (rank == 0) {
    // Check if a filename has been specified in the command
    if (argc < 2) {
      printf("Missing Filename\n");
      return (1);
    } else {
      filename = argv[1];
    }
    
    fp = fopen(filename, "r");
    
    if (mm_read_banner(fp, &matcode) != 0) {
      printf("Could not process Matrix Market banner.\n");
      exit(1);
    }
    
    /*  This is how one can screen matrix types if their application */
    /*  only supports a subset of the Matrix Market data types.      */
    if (mm_is_complex(matcode) && mm_is_matrix(matcode) &&
        mm_is_sparse(matcode)) {
      printf("Sorry, this application does not support ");
      printf("Matrix Market type: [%s]\n", mm_typecode_to_str(matcode));
      exit(1);
    }
    
    /* find out size of sparse matrix .... */
    if ((ret_code = mm_read_mtx_crd_size(fp, &nodes, &nodes, &nedges)) != 0)
      exit(1);
    
    // assumption is that the matrix is stored in one-based index
    
    printf("[HPC for CSE] Number of nodes %d and edges %d in webgraph %s \n",
           nodes, nedges, filename);
    
    G = (double *)calloc(nedges, sizeof(double));
    indices = (int *)calloc(nedges, sizeof(int));
    colptr = (int *)calloc(nodes + 1, sizeof(int));
    rowptr = (int *)calloc(nodes + 1,
                           sizeof(int));  // used to store matrix in compressed
    // sparse row format, row pointer
    x = (double *)calloc(nodes, sizeof(double));  // new solution vector x
    c = (int *)calloc(
                      nodes,
                      sizeof(int));  // number of nonzeros per column in G (out-degree);
    
    for (i = 0; i < nedges; i++) {
      if (fscanf(fp, "%d %d", &link, &colindex))
        ;
      link -= 1;      // matrix transfered to zero index
      colindex -= 1;  // matrix transfered to zero index
      indices[i] = link;
      if (colmatch == colindex) {
        localc += 1;
        G[i] = 1.0;
      } else {
        while ((colmatch + 1) != colindex) {
          c[j] = localc;
          colptr[j + 1] =
          colptr[j] + localc;  // index of G where new column starts
          j += 1;
          localc = 0;  // no new element
          c[j] = localc;
          colptr[j + 1] = colptr[j];  // index of G where new column starts
          colmatch += 1;              // new column
          localc = 0;                 // new localc
        }
        c[j] = localc;
        colptr[j + 1] =
        colptr[j] + localc;  // index of G where new column starts
        localc = 1;              // new localc
        j += 1;
        colmatch = colindex;  // new column
        G[i] = 1.0;
      }
    }
    c[j] = localc;  // entry for the last column in compressed column format
    colptr[j + 1] = nedges;  // toal number of nonzeros in the matrix G
    fclose(fp);
    
    //
    // transform matrix to compressed sparse row format for pagerank computation
    //
    convert_matrix(nodes, nedges, G, indices, colptr, rowptr);
    
    free(colptr);
  }
  
  err_code = MPI_Bcast(&nodes, 1, MPI_INT, 0, MPI_COMM_WORLD);
  assert(!err_code);
  err_code = MPI_Bcast(&nedges, 1, MPI_INT, 0, MPI_COMM_WORLD);
  assert(!err_code);
  
  if (rank != 0) {
    c = (int *)calloc(
                      nodes,
                      sizeof(int));  // number of nonzeros per column in G (out-degree); //
    // number of nonzeros per column in G (out-degree)
  }
  
  int nedges_send[size], nedges_displs[size];
  int nodes_send[size], nodes_displs[size];
  
  if (rank == 0) {
    // Calculate initial avg of nedges per rank
    double avg_nedges_per_rank = ((double)nedges) / size;
    for (i = 0; i < size; ++i) {
      if (i == 0) {
        nodes_displs[i] = 0;
        
        j = 1;
        // Iterate until difference between nedges for this rank and avg nedges
        // per rank is minimal OR until end of rows is reached
        while (pow(rowptr[j] - avg_nedges_per_rank, 2) >=
               pow(rowptr[j + 1] - avg_nedges_per_rank, 2) &&
               j < nodes - 1)
          ++j;
        
        nodes_send[i] = j;
      } else if (i < size - 1) {
        nodes_displs[i] = nodes_displs[i - 1] + nodes_send[i - 1];
        
        // Iterate until difference between nedges for this rank and avg nedges
        // per rank is minimal OR until end of rows is reached
        j = nodes_displs[i] + 1;
        while (pow(rowptr[j] - rowptr[nodes_displs[i]] - avg_nedges_per_rank,
                   2) >= pow(rowptr[j + 1] - rowptr[nodes_displs[i]] -
                             avg_nedges_per_rank,
                             2) &&
               j < nodes - 1)
          ++j;
        
        nodes_send[i] = j - nodes_displs[i];
      } else {
        nodes_displs[i] = nodes_displs[i - 1] + nodes_send[i - 1];
        nodes_send[i] = nodes - nodes_displs[i];
      }
    }
    
    for (i = 0; i < size - 1; ++i) {
      nedges_send[i] = rowptr[nodes_displs[i + 1]] - rowptr[nodes_displs[i]];
      
      if (i == 0)
        nedges_displs[i] = 0;
      else {
        nedges_displs[i] = nedges_displs[i - 1] + nedges_send[i - 1];
      }
    }
    nedges_send[size - 1] = nedges - rowptr[nodes_displs[size - 1]];
    if (size == 1) {
      nedges_displs[size - 1] = 0;
    } else {
      nedges_displs[size - 1] = nedges_displs[size - 2] + nedges_send[size - 2];
    }
  }
  
  err_code = MPI_Bcast(nodes_send, size, MPI_INT, 0, MPI_COMM_WORLD);
  assert(!err_code);
  err_code = MPI_Bcast(nodes_displs, size, MPI_INT, 0, MPI_COMM_WORLD);
  assert(!err_code);
  err_code = MPI_Bcast(nedges_send, size, MPI_INT, 0, MPI_COMM_WORLD);
  assert(!err_code);
  err_code = MPI_Bcast(nedges_displs, size, MPI_INT, 0, MPI_COMM_WORLD);
  assert(!err_code);
  
  err_code = MPI_Bcast(c, nodes, MPI_INT, 0, MPI_COMM_WORLD);
  assert(!err_code);
  
  int nodes_per_rank = nodes_send[rank];
  int nedges_per_rank = nedges_send[rank];
  
  if (rank == 0) {
    err_code =
    MPI_Scatterv(G, nedges_send, nedges_displs, MPI_DOUBLE, MPI_IN_PLACE,
                 nedges_per_rank, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    assert(!err_code);
    
    err_code =
    MPI_Scatterv(indices, nedges_send, nedges_displs, MPI_INT, MPI_IN_PLACE,
                 nedges_per_rank, MPI_INT, 0, MPI_COMM_WORLD);
    assert(!err_code);
    
    err_code =
    MPI_Scatterv(rowptr, nodes_send, nodes_displs, MPI_INT, MPI_IN_PLACE,
                 nodes_per_rank, MPI_INT, 0, MPI_COMM_WORLD);
    assert(!err_code);
    
  } else {
    G = (double *)calloc(nedges_per_rank, sizeof(double));
    indices = (int *)calloc(nedges_per_rank, sizeof(int));
    rowptr = (int *)calloc(nodes_per_rank + 1, sizeof(int));
    x = (double *)malloc(nodes_per_rank * sizeof(double));
    
    err_code = MPI_Scatterv(G, nedges_send, nedges_displs, MPI_DOUBLE, G,
                            nedges_per_rank, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    assert(!err_code);
    
    err_code =
    MPI_Scatterv(indices, nedges_send, nedges_displs, MPI_INT, indices,
                 nedges_per_rank, MPI_INT, 0, MPI_COMM_WORLD);
    assert(!err_code);
    
    err_code = MPI_Scatterv(rowptr, nodes_send, nodes_displs, MPI_INT, rowptr,
                            nodes_per_rank, MPI_INT, 0, MPI_COMM_WORLD);
    assert(!err_code);
    
    for (i = 1; i < nodes_per_rank; ++i) {
      rowptr[i] -= rowptr[0];
    }
    
    rowptr[nodes_per_rank] = nedges_per_rank;
    rowptr[0] = 0;
  }
  MPI_Pcontrol(-1, "InitialDistribution");
  
#ifdef BENCHMARK
  double *G_new = (double *)malloc(nedges_per_rank * sizeof(double));
  cblas_dcopy(nedges_per_rank, G, 1, G_new, 1);
  LSB_Rec_enable();
  for (run_id = 0; run_id < num_runs; ++run_id) {
    cblas_dcopy(nedges_per_rank, G_new, 1, G, 1);
#endif
    PageRank(G, indices, rowptr, c, nodes, nedges, nodes_send, nodes_displs, p,
             err, x, MPI_COMM_WORLD);
#ifdef BENCHMARK
  }
  LSB_Rec_disable();
  free(G_new);
#endif
  
  MPI_Pcontrol(1, "Assembling");
  if (rank == 0) {
    err_code =
    MPI_Gatherv(MPI_IN_PLACE, nodes_per_rank, MPI_DOUBLE, x, nodes_send,
                nodes_displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    assert(!err_code);
  } else {
    err_code = MPI_Gatherv(x, nodes_per_rank, MPI_DOUBLE, x, nodes_send,
                           nodes_displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    assert(!err_code);
  }
  
  // ************************************************
  // ********* Save PageRank vector x ***************
  // ************************************************
#ifndef BENCHMARK
  if (rank == 0) {
    fp = fopen("PageRank.csv", "w");
    fprintf(fp, "node,page-rank,in,out\n");
    for (j = 0; j < nodes; j++) {
      fprintf(fp, "%d,%e,%d,%d\n", j + 1, x[j], rowptr[j + 1] - rowptr[j], c[j]);
    }
    fclose(fp);
  }
#endif
  
  free(G);
  free(indices);
  free(rowptr);
  free(x);
  free(c);
  MPI_Pcontrol(-1, "Assembling");
#ifdef BENCHMARK
  LSB_Finalize();
#endif
  MPI_Finalize();
  return 0;
}

void convert_matrix(int nodes, int nedges, double *G, int *indices, int *colptr,
                    int *rowptr) {
  int *ja = (int *)calloc(nedges,
                          sizeof(int));  // used to store matrix in compressed
  // sparse row format,  column elements
  int *sumrow = (int *)calloc(
                              nodes + 1, sizeof(int));  // used to store matrix in compressed sparse row
  // format,  column elements
  double *G1 = (double *)calloc(nedges, sizeof(double));
  
  int i, j;
  
  for (i = 0; i < nodes; i++) {
    for (j = colptr[i]; j < colptr[i + 1]; j++) {
      sumrow[indices[j]] += 1;
    }
  }
  rowptr[0] = 0;
  for (i = 1; i < nodes + 1; i++) {
    rowptr[i] = rowptr[i - 1] + sumrow[i - 1];
  }
  for (i = 0; i < nodes; i++) {
    sumrow[i] = rowptr[i];
  }
  for (i = 0; i < nodes; i++) {
    for (j = colptr[i]; j < colptr[i + 1]; j++) {
      ja[sumrow[indices[j]]] = i;
      G1[sumrow[indices[j]]] = G[j];
      sumrow[indices[j]] += 1;
    }
  }
  for (i = 0; i < nodes; i++) {
    for (j = rowptr[i]; j < rowptr[i + 1]; j++) {
      G[j] = G1[j];
      indices[j] = ja[j];
    }
  }
  free(ja);
  free(G1);
  free(sumrow);
}
