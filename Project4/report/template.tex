\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{bbm}
\captionsetup[figure]{labelsep=space}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\input{assignment.sty}
\begin{document}


\setassignment
\setduedate{26 April 2021 (midnight)}

\serieheader{High-Performance Computing Lab for CSE}{2021}{Student: Niall Siegenheim}{Discussed with: Christoph Grötzbach, Maximilian Herde}{Solution for Project 4}{}
\newline

% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% --- Exercise 1 ----------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Ring maximum using MPI [10 Points]}
The goal of this exercise was to show the basic functionality of sending and receiving messages between ranks in MPI. 

In MPI, every rank (thread) is assigned an id. Here, each rank has two neighbours, defined by the neighbouring ids. The last rank neighbours the first rank, such that a ring is created. The goal is now for each rank to send its left neighbour's id to its right neighbour $ n $ times. In the end, all ranks will have received all rank ids in the ring, added up as a sum.

To do so, we first have to find the neighbours for each rank. This is easy to do, as each rank knows its own id in the standard \verb*|MPI_COMM_WORLD| communicator from a call to \verb*|MPI_Comm_rank| and can calculate its neighbouring ranks' ids. However, attention has to be paid to the first rank's left neighbour and the last rank's right neighbour. We can do the calculation in one step by simply using modulo operations.

To start the iteration, each rank sends its own id to its left neighbour using \verb*|MPI_Send|. Then, in a loop, it will receive what its right neighbour has sent it using \verb*|MPI_Recv|, add it to its total sum, and send it on to its left neighbour. This is repeated $ n $ times, which is known from \verb*|MPI_Comm_size|. At the end, each rank will have the same sum in its local memory, as every rank will have received all other ranks' ids.

 % -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% --- Exercise 2 ----------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Ghost cells exchange between neighboring processes [15 Points]}
Oftentimes when running PDE solvers in a two-dimensional domain, the iterations can be parallelised by segregating the whole domain into a subdomain for each MPI rank. However, to complete all computations correctly, each rank needs data of the nodes which neighbour its subdomain. These nodes are called ghost cells. Since these ghost cells are computed on a different rank, every rank needs to communicate with the owners of its neighbouring subdomain.

To do so, MPI offers the method \verb*|MPI_Cart_create|. Given the dimensions and periodicity, it returns a MPI communicator which arranges the available ranks in a grid (if possible). In our case specifically, this program is designed to run on 16 ranks such that there will be a four-by-four grid of subdomains. Also, we turn on periodicity, such that the subdomains on the borders of the domain will effectively face the subdomains on the other side's border. 

The next step is to find out who the neighbouring ranks are. For this, we can use the method \verb*|MPI_Cart_shift|. Using the \verb*|direction| and \verb*|displacement| arguments, it will return the rank who the source and the rank who the destination of this shift would be if run on this rank. This method has to be called twice, once for the left and right and once for the top and bottom neighbours of the grid. Special attention has to be paid to the signs of the two arguments to match the definition of the grid's origin elsewhere in the implementation.

Furthermore, we know the data is stored in a row-major fashion. Therefore, sending and receiving data at the top and bottom borders will not be an issue. However, these operations are non-trivial for the left and right borders, as it needs to skip elements in the data array. To make our life easier, we define a special MPI type \verb*|data_ghost| using \verb*|MPI_Type_vector|. This allows us to define a data type which consists of \verb*|block_count| \verb*|MPI_DOUBLE| elements, which are \verb*|stride| number of elements apart in the data array. MPI will then automatically take care of this when reading and writing this data type. In our case, \verb*|data_ghost| consists of six elements, which are eight elements apart in memory (taking into account ghost cells) in our data array.

Now, we can send and receive the ghost cells using \verb*|MPI_Send| and \verb*|MPI_Recv|. Doing so, we have to make sure to always receive the opposite side of what we have sent. For example, sending our top data will mean we receive data for the bottom of the domain. In total, we need four pairs of send-receive statements to communicate all relevant data. For the communication related to the top and bottom neighbours we can use normal vectors of \verb*|MPI_DOUBLE|s, while for the left and right neighbours we use our special \verb*|data_ghost| data type. In the end, we have the ghost cells of all the neighbouring ranks in our own local data array.

% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% --- Exercise 3 ----------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Parallelizing the Mandelbrot set using MPI [20 Points]}
The Mandelbrot set is defined as the set of complex numbers $ c \in \mathbb{C} $, for which the iteration $ z_0 = 0, z_{n+1} = z_n^2 + c $ converges. Using an image whose x-axis corresponds to the real axis and y-axis to the imaginary axis of the complex plane, we can represent every distinct complex number as a pixel whose colour indicates if it converges in the iteration. This yields image \ref{img:mandel}, where the white pixels represent the converging complex numbers.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{./fig/mandel/mandel.png}
	\caption{-- The Mandelbrot set visualised}
	\label{img:mandel}
\end{figure}

Since for every pixel it can be determined independently whether it converges or not, the computation of the Mandelbrot set is an obvious choice for parallelisation. 

First of all, our complex domain has to be divided into subdomains for each rank to work on, represented by the struct \verb*|Partition|. Creating a partition uses the convenience method \verb*|MPI_Dims_create| to determine how our available ranks can be laid out in a two-dimensional grid, if possible. Then, we can use the method \verb*|MPI_Cart_create| to create a new Cartesian coordinate system and assign each rank to a point on the grid, representing a subdomain. The method \verb*|MPI_Cart_coords| allows every rank to get its coordinates in the new grid.

Furthermore, every rank holds a struct of type \verb*|Domain|. This object relates the rank's subdomain to the global domain by calculating its global coordinates. They are easily determined, as we know the total image width and height and the position of each rank in the grid of subdomains. If the global domain is not perfectly divisible into the number of defined chunks, rank 0 will be assigned the leftover pixels.

Now, every rank can compute convergence for all of the pixels in its subdomain. Once it is finished, it will send its local data to the master rank (rank 0). Rank 0 iteratively waits for data from every other rank and adds it to the final image.

Looking at the benchmark in figure \ref{img:mandelbench}, it can be observed that performance scales quite well. Doubling the number of ranks roughly leads to halving the total time required. However, ranks slightly differ in the time it takes them finish. This is because the number of iterations needed for every pixel is different. If a pixel converges, all \verb*|MAX_ITERS| iterations have to be run. In the case it diverges, the loop can already be completed earlier once the iteration variable \verb*|c| leaves the convergence orbit. Therefore, regions with little to no converging pixels can be completed quicker than regions with a lot of converging pixels. This could be optimised by instead of statically arranging the grid before the algorithm starts, dynamically assigning new pixels during the computation to ranks to avoid some ranks being overloaded with work while other ranks are spinning.

\begin{figure}
	\centering
	\includegraphics[width=0.45\textwidth,angle=-90,origin=c]{./fig/mandel/perf.eps}
	\vspace*{-3cm}
	\caption{-- Performance plot for MPI Mandelbrot}
	\label{img:mandelbench}
\end{figure}

% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% --- Exercise 4B ---------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Option B: Parallel PageRank Algorithm and the Power method  [40 Points]}
The PageRank algorithm was originally used by Google to rank the relevance of webpages when searching for something. A webpage has a high rank in the search results if other pages with high rank link to it. The ranks are computed by performing random walks, where a theoretical \emph{surfer} starts on some webpage and surfs the web by following a link to get to a new webpage, and so on. Sometimes, with a probability $1-p$, it won't follow a link, but visit a completely new random website. From this, it can be determined how likely it is for a user to visit a specific webpage out of all webpages in the graph. Mathematically speaking, we can use the power method to find the so called Markov vector $x$, which contains the likelihoods for every webpage. The whole algorithm is described in full detail in the project description.

\subsection{Implementation}
\paragraph{Sparsity of G}
The matrix $G$ is the adjacency matrix for our webgraph. Since there are a lot of websites we want to rank, but every website only links to few other websites, it is very sparse. Therefore, to save memory, only non-zero matrix entries should be saved. To do so, we use the compressed sparse row (CSR) matrix format. It is made up of three arrays: An array containing all non-zero values, an array containing the column index of every non-zero value, and an array containing pointers to the first element of every row. That means the first two arrays contain as many elements as there are edges in the graph, while the last array has as many elements as there are nodes (websites) with an additional element providing the total number of edges.

In this case, we specifically chose a row-focussed storage format, as $ G $ is multiplied by a vector from the right during the power method. That means we calculate the dot-product between each row and this vector, calling for good row access performance.

\paragraph{What to parallelise}
The reduce calculation times especially for larger instances of $ G $, it makes sense to parallelise this algorithm using MPI in our case. We first have to analyse the calculations performed in algorithm \ref{alg:pagerank}. We will focus on the power method loop, as this takes up the majority of the computation time.

\begin{algorithm}
	\caption{Power Method for the PageRank algorithm} 
	$ G $: The adjusted adjacency matrix for PageRank of size $n_\mathrm{nodes}$.\\
	$ x $: The Markov vector, initialised to $ x_i = 1 / n_\mathrm{nodes} $.  It will contain the final ranking.\\
	$ z $: Another vector of size $ n_\mathrm{nodes} $, accounting for the jumps to other random pages  \\
	
	\vspace{0.2cm}
	\While{$\norm{x - x_s} > \epsilon $}{
		\vspace{0.2cm}
		$ x_s \gets x $ \\
		$ x \gets G x  + \mathbbm{1} \cdot (z \cdot x) $\\
		$ \mathrm{it} \gets \mathrm{it} + 1 $
	}
	
	\label{alg:pagerank}
\end{algorithm}

As we see, it makes sense to parallelise the computation of $Gx$, $\mathbbm{1} \cdot (z \cdot x)$ and $\norm{x - x_s}$.

\paragraph{Partitioning of G}
To parallelise the computation of $ x \gets Gx$ using MPI, it makes sense to partition $ G $ row-wise, such that every rank has a block of rows of $ G $ assigned to it and performs the computation $ G_\mathrm{local} x $ locally using CBLAS sparse operations. At the end of every iteration, \verb*|MPI_Allgatherv| is then used to put the $ x $ back together such that every rank has a completely up-tp-date version.

We want to partition $G$ such that every rank roughly has the same amount of work to do. As the work depends on the number of assigned edges and the number of edges may greatly differ between different rows, we cannot simply assign the same number of rows to every rank. Therefore, we propose a specialised partitioning algorithm. It will initially calculate how many edges every rank should be assigned on average. Then, it will iterate over all ranks and keep assigning consecutive rows until the difference between the number of edges assigned to this rank and the average number of edges per rank is minimized. The last rank will be assigned all leftover rows. There are smarter ways to do partitioning for sparse matrix-vector multiplication, but this simple approach is good-enough for our needs and already requires a relatively complicated implementation due to the unintuitive nature of the CSR matrix format.

The whole algorithm is run on the master rank, as this rank reads the web graph from the disk. After it is finished, the partitions of $ G $ are communicated to the corresponding ranks by first sending the partitioning info (no. of nodes, edges for every rank) using \verb*|MPI_Bcast| to allocate local memory and subsequently sending the actual data of $ G $ using \verb*|MPI_Gatherv|.

\paragraph{Calculation of dot product and norm}
Theoretically, all ranks could calculate $\mathbbm{1} \cdot (z \cdot x)$ and $\norm{x - x_s}$ themselves as this does not require any data from partitioned matrix $ G $. However, it makes sense to parallelise these relatively simple calculations by splitting the vectors into partitions too. Then, only a local part is computed on every rank using CBLAS and subsequently the total norm/dot-product is calculated by adding up the local results and communicated to all ranks using \verb*|MPI_Allreduce|.

\subsection{Benchmarking and results}
To test our implementation, we use the \verb*|LiveJournal1|, \verb*|Orkut| and \verb*|Friendster| webgraphs from the Stanford Network Analysis Platform collection, visualized in collection of plots \ref{tab:matrices}. The rankings for all three datasets can be seen in table \ref{tab:page_ranks}. Curiously, the number of outgoing edges seem to be incorrect in our data, as at least \verb*|Orkut| and \verb*|Friendster| are undirected graphs and should therefore have the same number of incoming and outgoing edges for every node. However, we were not able to spot the issue in our code.

\begin{table}[h]
	\centering
	\begin{subtable}[h]{0.33\textwidth}
		\centering
		\includegraphics[width=\textwidth,origin=c]{./fig/pagerank/matrix_livejournal}
		\caption{soc-LiveJournal1}
		\label{tab:matrix_live_journal}
	\end{subtable}
	\begin{subtable}[h]{0.33\textwidth}
		\centering
		\includegraphics[width=\textwidth,origin=c]{./fig/pagerank/matrix_orkut}
		\caption{com-Orkut}
		\label{tab:matrix_orkut}
	\end{subtable}
	\begin{subtable}[h]{0.28\textwidth}
		\centering
		\includegraphics[width=\textwidth,origin=c]{./fig/pagerank/matrix_friendster}
		\caption{com-Friendster}
		\label{tab:matrix_friendster}
	\end{subtable}
	
	\caption{Visualization of adjacency matrix for SNAP web graphs.}
	\label{tab:matrices}
\end{table}

\begin{table}[h]
	\centering
	\begin{subtable}[h]{0.49\textwidth}
		\centering
		\begin{tabular}{|c|c|c|c|}
			\hline
			node & PageRank & in & out \\
			\hline \hline
			10010 & 2.596876e-04 & 20293 & 2596 \\
			37357 & 1.320235e-04 & 15163 & 391 \\
			88 & 1.291800e-04 & 13050 & 2168 \\
			40749 & 7.068890e-05 & 8428 & 884 \\
			102294 & 5.498140e-05 & 5597 & 1813 \\
			\hline
		\end{tabular}
		\caption{soc-LiveJournal1}
		\label{tab:live_journal}
	\end{subtable}
	\hfill
	\begin{subtable}[h]{0.49\textwidth}
		\centering
		\begin{tabular}{|c|c|c|c|}
			\hline
			node & PageRank & in & out \\
			\hline \hline
			65603986 & 4.948446e-05 & 864 & 13 \\
			65597945 & 3.480060e-05 & 1176 & 52 \\
			65604608 & 3.174680e-05 & 194 & 2 \\
			64593673 & 3.018574e-05 & 2878 & 53 \\
			64524815 & 2.907642e-05 & 2898 & 30 \\
			\hline
		\end{tabular}
		\caption{com-Friendster}
		\label{tab:friendster}
	\end{subtable}
	
	
	\bigskip
	\begin{subtable}[h]{\textwidth}
		\centering
		\begin{tabular}{|c|c|c|c|}
			\hline
			node & PageRank & in & out \\
			\hline \hline
			3036689 & 9.743328e-05 & 248 & 14 \\
			2994689 & 9.148665e-05 & 205 & 3 \\
			3065023 & 8.961949e-05 & 194 & 1 \\
			3065896 & 8.590623e-05 & 371 & 0 \\
			3065208 & 8.585101e-05 & 19 & 0 \\
			\hline
		\end{tabular}
		\caption{com-Orkut}
		\label{tab:orkut}
	\end{subtable}
	\caption{Five highest ranking nodes for the corresponding graphs with in- and out-degrees. Node indexing starts at 1.}
	\label{tab:page_ranks}
\end{table}

To benchmark our implementation, we use the library \verb*|liblsb| as well as the built-in MPI timing features to determine the MPI overhead. As in the given sequential implementation, we only time the power method loop, but do not time any of the start-up calculations. All of our runs are performed on Euler V nodes with Intel Xeon Gold 5118 processors with 24 cores. Speed-up plots are given in plots \ref{tab:performace_live_journal}, \ref{tab:performance_friendster} and \ref{tab:performance_orkut} for a speed-up relative to the sequential (non-CBLAS) and to the parallel implementation run on one rank are given. Sadly, our implementation crashed using the dataset \verb*|Friendster| for numerous numbers of ranks due to memory limitations. Because of the long runtime on this dataset and difficult debugging, we were not able to identify a fix for this issue.

\begin{table}[h]
	\centering
	\begin{subtable}[h]{\textwidth}
		\centering
		\includegraphics[width=0.65\textwidth,origin=c]{./fig/pagerank/performance_livejournal_balanced}
		\caption{soc-LiveJournal1}
		\label{tab:performace_live_journal}
	\end{subtable}

	\bigskip
	\begin{subtable}[h]{\textwidth}
		\centering
		\includegraphics[width=0.65\textwidth,origin=c]{./fig/pagerank/performance_friendster}
		\caption{com-Friendster}
		\label{tab:performance_friendster}
	\end{subtable}
	
	\bigskip
	\begin{subtable}[h]{\textwidth}
		\centering
		\includegraphics[width=0.65\textwidth,origin=c]{./fig/pagerank/performance_orkut_balanced}
		\caption{com-Orkut}
		\label{tab:performance_orkut}
	\end{subtable}
	\caption{Speed-up plots for different web graphs for PageRank using MPI.}
	\label{tab:performance}
\end{table}

For all three graphs, performance scales quite well, still leaving quite big differences between each other. This is due to the size of the input data and possible caching effects as well as the variance in the distribution of the data within the matrix. We can also observe spurious jumps when we scale from one to two nodes (i.e. when the no. of ranks exceeds 24).

\begin{table}[h]
	\centering
	\begin{subtable}[h]{\textwidth}
		\centering
		\includegraphics[width=0.65\textwidth,origin=c]{./fig/pagerank/percentage_livejournal_balanced}
		\caption{soc-LiveJournal1}
		\label{tab:percentage_live_journal}
	\end{subtable}
	
	\bigskip
	\begin{subtable}[h]{\textwidth}
		\centering
		\includegraphics[width=0.65\textwidth,origin=c]{./fig/pagerank/percentage_friendster}
		\caption{com-Friendster}
		\label{tab:percentage_friendster}
	\end{subtable}
	
	\bigskip
	\begin{subtable}[h]{\textwidth}
		\centering
		\includegraphics[width=0.65\textwidth,origin=c]{./fig/pagerank/percentage_orkut_balanced}
		\caption{com-Orkut}
		\label{tab:percentage_orkut}
	\end{subtable}
	\caption{Percentages MPI communication takes up during power method of PageRank algorithm.}
	\label{tab:percentage}
\end{table}

Furthermore, the share MPI communication takes up during the iterations is depicted in table \ref{tab:percentage}. Especially for the two smaller datasets \verb*|LiveJournal1| and \verb*|Orkut|, it is quite surprising how much the communication takes up. As the total problem size stays the same for an increasing number of ranks, the computations take up less time while communication time actually increases. \verb*|Friendster| is not as strongly affected by this because the total problem size is a lot bigger.

Performance could be further improved by finding a better algorithm for the partitioning of $ G $ as well as possibly employing hybrid parallelisation using both MPI for inter-socket communication and OpenMP for inter-core communication. We performed some experiments for this, however our results were rather confusing, e.g. single-core performance was better than dual-core performance. Therefore, the results are not shown here.

\end{document}
