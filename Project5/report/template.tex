\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\usepackage{amsmath}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx} 
\usepackage[page]{appendix}
\usepackage{float}
\input{assignment.sty}
\begin{document}


\setassignment
\setduedate{10.05.2021 (midnight)}

\serieheader{High-Performance Computing Lab}{2021}{Student: Niall Siegenheim}{Discussed with: Christoph Grötzbach, Maximilian Herde}{Solution for Project 5}{}
\newline

\section*{Task 2:  Construct adjacency matrices from connectivity data}
\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.33\textwidth}
		\centering
		\includegraphics[height=5cm]{./plots/2/vietnam.pdf}
		\caption{Vietnam}
	\end{subfigure}
	\begin{subfigure}[h]{0.66\textwidth}
		\centering
		\includegraphics[height=5cm]{./plots/2/norway.pdf}
		\caption{Norway}
	\end{subfigure}
	\caption{Visualization of graphs}
	\label{fig:countries}
\end{figure}

The adjacency matrix and coordinates list are read from the corresponding \verb|csv| files. The adjacency matrix can then be constructed by using the \verb|sparse| command and made symmetric using \verb|W_bar = 0.5 * (W + W.')|.
Now, we can visualize the provided countries using the provided method \verb|gplotg|, which takes an adjacency matrix and a list of coordinates for every node. The results for Norway and Vietnam can be seen in figure \ref{fig:countries}.
All of the other provided countries are to be found in the appendix.

\section*{Task 3: Implement various graph partitioning algorithms}

\paragraph{Spectral bisection}
Spectral graph bisection simply works by calculating the eigenvector associated to the second smallest eigenvalue of the graph Laplacian matrix, the Fiedler eigenvector.
Then, we use the median value of all its entries as threshold for the partitioning, as every node is associated with an entry in the Fiedler eigenvector.

\paragraph{Inertial bisection}
In contrast to spectral bisection, inertial bisection exclusively uses the nodes' coordinates for partitioning.
We find the partitioning plane by getting the eigenvector associated with the smallest eigenvalue of the covariance matrix around the center of mass. This vector cuts through the point cloud dividing it into two equally big partitions.
Mathematically, this is equal to minimizing the sum of squared differences of points along a plane and then partitioning by the orthogonal to this plane.
However, we experienced difficulties implementing the algorithm correctly, as there is conflicting information on how exactly the matrix and plane should be constructed.

\begin{table}[h]
	\centering
	\begin{tabular}{l|r|r|r|r} \hline\hline
		Mesh           & Coordinate & METIS 5.0.2 & Spectral & Inertial \\ \hline
		mesh1e1        & 18         & 17          & 18       & 19       \\
		mesh2e1        & 37         & 37          & 35       & 47       \\
		netz4504\_dual & 25         & 20          & 23       & 30       \\
		stufe          & 16         & 35          & 16       & 16       \\
		\hline \hline
	\end{tabular}
	\caption{Edge-cut results for various bi-partitioning methods}
	\label{table:bisection}
\end{table}

The number of cuts required for the different bisection methods are shown in table \ref{table:bisection}.
As inertial bisection does not take the edges between nodes into account, it will often lead to more cuts than other methods.

\section*{Task 4: Recursively bisecting meshes}
By applying bisection methods recursively, we can extend the number of partitions from two to any power of two.
That means once a mesh is partitioned into two parts, we apply the same partition method to those two parts again, giving us four partitions, and so on.

\begin{table}[h]
	\centering
	\begin{tabular}{l|r|r|r|r} \hline\hline
		Case     & Spectral & METIS 5.0.2 & Coordinate & Inertial \\ \hline
		mesh3e1  & 78       & 74          & 70         & 75       \\
		airfoil1 & 398      & 333         & 618        & 577      \\
		3elt     & 469      & 383         & 697        & 880      \\
		barth4   & 550      & 463         & 875        & 891      \\
		crack    & 883      & 800         & 1150       & 1061     \\ \hline \hline
	\end{tabular}
	\caption{Edge-cut results for recursive bi-partitioning using $p=8$}
	\label{table:Rec_bisection_8}
\end{table}

\begin{table}[h]
	\centering
	\begin{tabular}{l|r|r|r|r} \hline\hline
		Case     & Spectral & METIS 5.0.2 & Coordinate & Inertial \\ \hline
		mesh3e1  & 129      & 113         & 109        & 116      \\
		airfoil1 & 633      & 565         & 817        & 897      \\
		3elt     & 752      & 693         & 808        & 1342     \\
		barth4   & 841      & 713         & 1174       & 1350     \\
		crack    & 1419     & 1253        & 1632       & 1618     \\ \hline \hline
	\end{tabular}
	\caption{Edge-cut results for recursive bi-partitioning using $p=16$}
	\label{table:Rec_bisection_16}
\end{table}

The generated cuts different partition methods applied recursively for eight and 16 partitions can be observed in tables \ref{table:Rec_bisection_8} and \ref{table:Rec_bisection_16}, respectively.
It can be observed that the METIS and spectral algorithms generally generate substanitally fewer cuts than the coordinate and inertial methods.

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{./plots/4/inertial.pdf}
		\caption{Inertial}
	\end{subfigure}
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{./plots/4/spectral.pdf}
		\caption{Spectral}
	\end{subfigure}

	\vspace{0.5cm}

	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{./plots/4/metis.pdf}
		\caption{METIS}
	\end{subfigure}
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{./plots/4/coordinate.pdf}
		\caption{Coordinate}
	\end{subfigure}

	\caption{Comparison of different recursive bisection strategies}
	\label{fig:rec_bisection_crack}

\end{figure}

Interestingly, the different recursively applied methods generate very different patterns, as observed in figure \ref{fig:rec_bisection_crack}.
The inertial and coordinate methods generate straight cuts, while the METIS and spectral methods are more flexible.
This could be one of the reasons they generate fewer cuts.

\section*{Task 5: Comparing recursive bisection to direct $k$-way partitioning}
The METIS package offers two sophisticated methods for partitioning: Recursive bipartitioning and k-way partitioning.

\begin{table}[h]
	\centering
	\begin{subtable}[h]{\textwidth}
		\centering
		\begin{tabular}{l|r|r|r|r|r|r|r} \hline\hline
			Method          & Luxemburg & usroads-48 & Greece & Switzerland & Vietnam & Norway & Russia \\ \hline
			Recursive       & 198       & 605        & 294    & 709         & 249     & 297    & 586    \\
			Direct Multiway & 174       & 553        & 275    & 678         & 261     & 267    & 530    \\ \hline\hline
		\end{tabular}
		\caption{16 partitions}
	\end{subtable}

	\vspace{0.5cm}

	\begin{subtable}[h]{\textwidth}
		\centering
		\begin{tabular}{l|r|r|r|r|r|r|r} \hline\hline
			Method          & Luxemburg & usroads-48 & Greece & Switzerland & Vietnam & Norway & Russia \\ \hline
			Recursive       & 321       & 968        & 518    & 1051        & 450     & 456    & 1030   \\
			Direct Multiway & 291       & 932        & 487    & 1046        & 412     & 454    & 915    \\ \hline\hline
		\end{tabular}
		\caption{32 partitions}
	\end{subtable}

	\caption{Comparing the number of cut edges for recursive bisection and direct multiway partitioning in METIS 5.0.2}
	\label{table:Compare_Metis}
\end{table}

As observed in table \ref{table:Compare_Metis}, direct multiway partitioning generates slightly fewer cuts.
This is likely due to a more global approach to partitioning, as the result does not depend as highly on the first few partitioning decisions.
Therefore, it is more robust.

\begin{figure}
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/5/luxembourg_rec.pdf}
		\caption{Recursive partitioning}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/5/luxembourg_kway.pdf}
		\caption{k-way partitioning}
	\end{subfigure}

	\caption{Comparison of METIS bisection for Luxembourg's roads for 32 partitions}
	\label{fig:luxembourg}
\end{figure}

Figure \ref{fig:luxembourg} visualizes the two different partitioning methods applied to the Luxembourg road network. Even though they work differently, the partitioning looks quite similar at first glance. In the appendix, the same comparison is shown for the US and Russian road network.

\section*{Task 6: Utilizing graph eigenvectors}
\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/6/a/1.pdf}
		\caption{Eigenvector associated with $\lambda_2$}
	\end{subfigure}
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/6/a/2.pdf}
		\caption{Eigenvector associated with $\lambda_3$}
	\end{subfigure}

	\caption{Eigenvectors associated with the second smallest eigenvalue $\lambda_2$ and third smallest eigenvalue $\lambda_3$}
	\label{fig:eigenvectors}
\end{figure}

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/6/b/3elt.pdf}
		\caption{3elt}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/6/b/barth4.pdf}
		\caption{barth4}
	\end{subfigure}

	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/6/b/crack.pdf}
		\caption{crack}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/6/b/mesh3e1.pdf}
		\caption{mesh3e1}
	\end{subfigure}
	\caption{Partitioning of graphs with every vertex' value of the eigenvector associated with the second smallest eigenvalue projected into the z-axis}
	\label{fig:3dplot}
\end{figure}

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/6/c/3elt.pdf}
		\caption{3elt}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/6/c/barth4.pdf}
		\caption{barth4}
	\end{subfigure}

	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/6/c/crack.pdf}
		\caption{crack}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./plots/6/c/mesh3e1.pdf}
		\caption{mesh3e1}
	\end{subfigure}

	\caption{Partitioning of graphs using every vertex values of the eigenvector assoicated with second and third smallest eigenvalues}
	\label{fig:v2v3}
\end{figure}

\begin{enumerate}
	\item Plotting the entries of the eigenvector associated with the first smallest eigenvalue of the graph Laplacian matrix, they are all constant, as observed in figure \ref{fig:eigenvectors}.
	      As every row in the Laplacian matrix adds up to zero, $ Lv = 0, \: v = [ 1, \dots, 1 ]^T $.
	      Because $ L $ is semi-positive definite and $ Lv = \lambda v $, the smallest eigenvalue is $ \lambda = 0 $, whose associated eigenvector is $ v $.
	      Therefore, the eigenvector associated to the smallest eigenvalue has constant entries.

	      The eigenvector associated with the second smallest eigenvalue shows a significant distinction between nodes associated with an entry bigger than a constant and nodes associated with an entry smaller than a constant.
	      As we threshold around the median of all entries in spectral bisection, we can find a good partitioning.
	\item We can plot the partitioning of a graph using its coordinates in the xy-plane and the plot the eigenvector entries associated with every node in the z-axis, as shown in figure \ref{fig:3dplot}.
	      This makes it easy to understand how spectral bisection works: All nodes which have an entry in the eigenvector bigger than the median belong to one part of the bisection, while all other nodes belong to the other part.
	\item Figure \ref{fig:v2v3} shows an alternative visualization of a graph only using the eigenvectors associated with the second and third smallest eigenvalues of the graph Laplacian matrix. This is useful as not all graph problems have xy coordinates associated with them but you still want to get an intuition for the topology of a graph.
\end{enumerate}

\clearpage
\begin{appendices}
	\section*{Task 2:  Construct adjacency matrices from connectivity data}
	\begin{figure}[H]
		\begin{subfigure}[h]{0.49\textwidth}
			\centering
			\includegraphics[height=4cm]{./plots/2/switzerland.pdf}
			\caption{Switzerland}
		\end{subfigure}
		\begin{subfigure}[h]{0.49\textwidth}
			\centering
			\includegraphics[height=4cm]{./plots/2/greece.pdf}
			\caption{Greece}
		\end{subfigure}

		\vspace{0.5cm}

		\begin{subfigure}[h]{\textwidth}
			\centering
			\includegraphics[height=8cm]{./plots/2/gb.pdf}
			\caption{Great Britain}
		\end{subfigure}

		\vspace{0.5cm}

		\begin{subfigure}[h]{\textwidth}
			\centering
			\includegraphics[height=4cm]{./plots/2/russia.pdf}
			\caption{Russia}
		\end{subfigure}

		\caption{Visualization of country graphs}

	\end{figure}

	\section*{Task 5: Comparing recursive bisection to direct $k$-way partitioning}
	\begin{figure}[H]
		\centering
		\begin{subfigure}[h]{0.7\textwidth}
			\centering
			\includegraphics[width=\textwidth]{./plots/5/us_rec.pdf}
			\caption{Recursive partitioning}
		\end{subfigure}

		\vspace{0.5cm}

		\begin{subfigure}[h]{0.7\textwidth}
			\centering
			\includegraphics[width=\textwidth]{./plots/5/us_kway.pdf}
			\caption{k-way partitioning}
		\end{subfigure}

		\caption{Comparison of METIS bisection for US roads (lower 48 states) for 32 partitions}
	\end{figure}

	\begin{figure}[H]
		\centering
		\begin{subfigure}[h]{0.9\textwidth}
			\centering
			\includegraphics[width=\textwidth]{./plots/5/russia_rec.pdf}
			\caption{Recursive partitioning}
		\end{subfigure}

		\vspace{0.5cm}

		\begin{subfigure}[h]{0.9\textwidth}
			\centering
			\includegraphics[width=\textwidth]{./plots/5/russia_kway.pdf}
			\caption{k-way partitioning}
		\end{subfigure}

		\caption{Comparison of METIS bisection for Russia for 32 partitions}
	\end{figure}
\end{appendices}

\end{document}
