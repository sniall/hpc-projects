function [part1,part2] = bisection_inertial(A,xy,picture)
% bisection_inertial : Inertial partition of a graph.
%
% [p1,p2] = bisection_inertial(A,xy) returns a list of the vertices on one side of a partition
%     obtained by bisection with a line or plane normal to a moment of inertia
%     of the vertices, considered as points in Euclidean space.
%     Input A is the adjacency matrix of the mesh (used only for the picture!);
%     each row of xy is the coordinates of a point in d-space.
%
% bisection_inertial(A,xy,1) also draws a picture.
%
% See also PARTITION


% disp(' ');
% disp(' HPC Lab for CSE at ETH:   ');
% disp(' Implement inertial bisection');
% disp(' ');


% Steps
% 1. Calculate the center of mass.
com = sum(xy, 1) / size(xy, 1);
xbar = com(1);
ybar = com(2);

% 2. Construct the matrix M.
Sxx = sum((xy(:, 1) - xbar).^2);
Syy = sum((xy(:, 2) - ybar).^2);
Sxy = sum((xy(:, 1) - xbar) .* (xy(:, 2) - ybar));
 
% 3. Calculate the smallest eigenvector of M. 
M = [ Sxx Sxy;
      Sxy Syy ];

[V, D] = eig(M);
[~, ind] = sort(diag(D));
Vs = V(:, ind);

smallest_eigv = Vs(:, 1);

% 4. Find the line L on which the center of mass lies.
l_normal = [ smallest_eigv(2) -smallest_eigv(1) ];

% 5. Partition the points around the line L.
[part1, part2] = partition(xy, l_normal);

if picture == 1
    gplotpart(A,xy,part1);
    title('Inertial bisection using the Fiedler Eigenvector');
end

end
