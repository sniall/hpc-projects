function [cut_recursive,cut_kway] = Bench_metis(picture)
% Compare recursive bisection and direct k-way partitioning,
% as implemented in the Metis 5.0.2 library.

%  Add necessary paths
addpaths_GP;

% Graphs in question
cases = {
    'luxembourg_osm.mat',
    'usroads.mat',
    'GR_graph.mat',
    'CH_graph.mat',
    'VN_graph.mat',
    'NO_graph.mat',
    'RU_graph.mat'
};

nc = length(cases);
maxlen = 0;
for c = 1:nc
    if length(cases{c}) > maxlen
        maxlen = length(cases{c});
    end
end

for c = 1:nc
    fprintf('.');
    sparse_matrices(c) = load(cases{c});
end

for c = 1:nc
    spacers  = repmat('.', 1, maxlen+3-length(cases{c}));
    [params] = Initialize_case(sparse_matrices(c));
end

for c = 1:nc
    % Steps
    % 1. Initialize the cases
    [params] = Initialize_case(sparse_matrices(c));
    W = params.Adj;
    coords = params.coords;
    
    if issymmetric(W) == false
        W = 0.5 * (W + transpose(W))
    end

    % 2. Call metismex to
    %     a) Recursively partition the graphs in 16 and 32 subsets.
    [map_rec_16, edgecut_rec_16] = metismex('PartGraphRecursive', W, 16);
    [map_rec_32, edgecut_rec_32] = metismex('PartGraphRecursive', W, 32);

    %     b) Perform direct k-way partitioning of the graphs in 16 and 32 subsets.
    [map_kway_16, edgecut_kway_16] = metismex('PartGraphKway', W, 16);
    [map_kway_32, edgecut_kway_32] = metismex('PartGraphKway', W, 32);

    fprintf('16 %s: rec %d      kway %d\n', cases{c}, edgecut_rec_16, edgecut_kway_16)
    fprintf('32 %s: rec %d      kway %d\n\n', cases{c}, edgecut_rec_32, edgecut_kway_32)
    
    % 3. Visualize the results for 32 partitions.
%     if strcmp(cases{c}, 'luxembourg_osm.mat')
%         gplotmap(W, coords, map_rec_32);
%         export_fig('luxembourg_rec.pdf')
%         gplotmap(W, coords, map_kway_32);
%         export_fig('luxembourg_kway.pdf')
%     end
%     
%     if strcmp(cases{c}, 'usroads.mat')
%         ind = find(coords(:,1) < 0 & coords(:,1) > -130 & coords(:,2) > 20);
%         gplotmap(W(ind, ind), coords(ind, :), map_rec_32(ind));
%         export_fig('us_rec.pdf')
%         gplotmap(W(ind, ind), coords(ind, :), map_kway_32(ind));
%         export_fig('us_kway.pdf')
%     end
%     
%     if strcmp(cases{c}, 'RU_graph.mat')
%         gplotmap(W, coords, map_rec_32);
%         export_fig('russia_rec.pdf');
%         gplotmap(W, coords, map_kway_32);
%         export_fig('russia_kway.pdf')
%     end
end

end