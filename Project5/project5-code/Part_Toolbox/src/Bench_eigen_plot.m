% Visualize information from the eigenspectrum of the graph Laplacian
%
% D.P & O.S for HPC Lab for CSE at ETH Zurich

% add necessary paths
addpaths_GP;

% Graphical output at bisection level
picture = 0;

% Cases under consideration
% load airfoil1.mat;
% load 3elt.mat;
% load barth4.mat;
% load mesh3e1.mat;
load crack.mat;

% Initialize the cases
W      = Problem.A;
coords = Problem.aux.coord;

% Steps
% 1. Construct the graph Laplacian of the graph in question.
L = diag(sum(W, 1)) - W;

% 2. Compute eigenvectors associated with the smallest eigenvalues.
[V, ~] = eigs(L, 3, 'smallestabs');

% 3. Perform spectral bisection.
fiedler = V(:,2);
threshold = median(fiedler);
part1 = find(fiedler > threshold);
part2 = find(fiedler <= threshold);

% 4. Visualize:
%   i.   The first and second eigenvectors.
% plot(linspace(1, size(V(:,1), 1), size(V(:,1), 1)), V(:,1))
% axis([ 0 size(V(:,1), 1)+1 0 1.2 * V(1,1) ])
% pause;
% cla;
% plot(linspace(1, size(V(:,2), 1), size(V(:,2), 1)), V(:,2))
% pause;

%   ii.  The second eigenvector projected on the coordinate system space of graphs.
[ coords V(:,2) ]
gplotpart3d(W, [ coords V(:,2) ], part1, [0.2, 0.2, 0.2], [0.7, 0.7, 0.7], 'red');
pause;

%   iii. The spectral bi-partitioning results using the spectral coordinates of each graph.
gplotpartspectral(W, [ V(:,2) V(:,3) ], part1, [0.7, 0.7, 0.7], [0.2, 0.2, 0.2], 'red')






