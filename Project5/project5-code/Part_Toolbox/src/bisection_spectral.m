function [part1,part2] = bisection_spectral(A,xy,picture)
% bisection_spectral : Spectral partition of a graph.
%
% [part1,part2] = bisection_spectral(A) returns a partition of the n vertices
%                 of A into two lists part1 and part2 according to the
%                 spectral bisection algorithm of Simon et al:  
%                 Label the vertices with the components of the Fiedler vector
%                 (the second eigenvector of the Laplacian matrix) and partition
%                 them around the median value or 0.



% disp(' ');
% disp(' HPC Lab for CSE at ETH:   ');
% disp(' Implement spectral bisection');
% disp(' ');


% Steps
% 1. Construct the Laplacian.
if issymmetric(A) == false
    A = 0.5 * (A + A.')
end

L = diag(sum(A, 1)) - A;

% 2. Calculate its eigensdecomposition.
[V, ~] = eigs(L, 2, 'smallestabs');

% 3. Label the vertices with the components of the Fiedler vector.
fiedler = V(:,2);

% 4. Partition them around their median value, or 0.
threshold = median(fiedler);
part1 = find(fiedler > threshold);
part2 = find(fiedler <= threshold);

if picture == 1
    gplotpart(A,xy,part1);
    title('Spectral bisection using the Fiedler Eigenvector');
end

end
