% Benchmark for recursively partitioning meshes, based on various
% bisection approaches
%
% D.P & O.S for HPC Lab for CSE at ETH Zurich



% add necessary paths
addpaths_GP;
nlevels_a = 3;
nlevels_b = 4;

fprintf('       *********************************************\n')
fprintf('       ***  Recursive graph bisection benchmark  ***\n');
fprintf('       *********************************************\n')

% load cases
cases = {
    'airfoil1.mat';
    '3elt.mat';
    'barth4.mat';
    'mesh3e1.mat';
    'crack.mat';
    };

nc = length(cases);
maxlen = 0;
for c = 1:nc
    if length(cases{c}) > maxlen
        maxlen = length(cases{c});
    end
end

for c = 1:nc
    fprintf('.');
    sparse_matrices(c) = load(cases{c});
end


fprintf('\n\n Report Cases         Nodes     Edges\n');
fprintf(repmat('-', 1, 40));
fprintf('\n');
for c = 1:nc
    spacers  = repmat('.', 1, maxlen+3-length(cases{c}));
    [params] = Initialize_case(sparse_matrices(c));
    fprintf('%s %s %10d %10d\n', cases{c}, spacers,params.numberOfVertices,params.numberOfEdges);
end

%% Create results table
fprintf('\n%7s %16s %20s %16s %16s\n','Bisection','Spectral','Metis 5.0.2','Coordinate','Inertial');
fprintf('%10s %10d %6d %10d %6d %10d %6d %10d %6d\n','Partitions',8,16,8,16,8,16,8,16);
fprintf(repmat('-', 1, 100));
fprintf('\n');


for c = 1:nc
    spacers = repmat('.', 1, maxlen+3-length(cases{c}));
    fprintf('%s %s', cases{c}, spacers);
    sparse_matrix = load(cases{c});
    

    % Recursively bisect the loaded graphs in 8 and 16 subgraphs.
    % Steps
    % 1. Initialize the problem
    [params] = Initialize_case(sparse_matrices(c));
    W      = params.Adj;
    coords = params.coords; 
    
    % 2. Recursive routines
    % 3. Calculate number of cut edges
    % i. Spectral
    [map, res_spectral_8, sepW] = rec_bisection('bisection_spectral', 3, W, coords, 0);
    
    [map, res_spectral_16, sepW] = rec_bisection('bisection_spectral', 4, W, coords, 0);
    
%     if strcmp(cases{c}, 'crack.mat')
%         gplotmap(W, coords, map)
%         ax = gca;
%         exportgraphics(ax, 'spectral.pdf', 'BackgroundColor', 'none', 'ContentType', 'vector');
%         pause;
%     end
    
    % ii. Metis
    [map, res_metis_8, sepW] = rec_bisection('bisection_metis', 3, W, coords, 0);
    
    [map, res_metis_16, sepW] = rec_bisection('bisection_metis', 4, W, coords, 0);
    
%     if strcmp(cases{c}, 'crack.mat')
%         gplotmap(W, coords, map)
%         ax = gca;
%         exportgraphics(ax, 'metis.pdf', 'BackgroundColor', 'none', 'ContentType', 'vector');
%         pause;
%     end
%     
    
    % iii. Coordinate 
    [map, res_coord_8, sepW] = rec_bisection('bisection_coordinate', 3, W, coords, 0);
    
    [map, res_coord_16, sepW] = rec_bisection('bisection_coordinate', 4, W, coords, 0);
    
%     if strcmp(cases{c}, 'crack.mat')
%         gplotmap(W, coords, map)
%         ax = gca;
%         exportgraphics(ax, 'coordinate.pdf', 'BackgroundColor', 'none', 'ContentType', 'vector');
%         pause;
%     end
    
    % iv. Inertial
    [map, res_inertial_8, sepW] = rec_bisection('bisection_inertial', 3, W, coords, 0);
    
    [map, res_inertial_16, sepW] = rec_bisection('bisection_inertial', 4, W, coords, 0);
    
%     % 4. Visualize the partitioning result
%     if strcmp(cases{c}, 'crack.mat')
%         gplotmap(W, coords, map)
%         ax = gca;
%         exportgraphics(ax, 'inertial.pdf', 'BackgroundColor', 'none', 'ContentType', 'vector');
%         pause;
%     end
    
    fprintf('%6d %6d %10d %6d %10d %6d %10d %6d\n', ...
            length(res_spectral_8), length(res_spectral_16), ...
            length(res_metis_8), length(res_metis_16), ...
            length(res_coord_8), length(res_coord_16), ...
            length(res_inertial_8), length(res_inertial_16));
    
end
