% Script to load .csv lists of adjacency matrices and the corresponding 
% coordinates. 
% The resulting graphs should be visualized and saved in a .csv file.
% D.P & O.S for HPC Lab for CSE at ETH Zurich

addpaths_GP;

% Steps
% 1. Load the .csv files
adj = csvread('Countries/csv/RU-40527-adj.csv', 1, 0);
coords = csvread('Countries/csv/RU-40527-pts.csv', 1, 0);

% 2. Construct the adjaceny matrix (NxN).
Problem.A = sparse(adj(:, 1), adj(:, 2), 1);

if issymmetric(Problem.A) == false
        Problem.A = 0.5 * (Problem.A + transpose(Problem.A))
end

Problem.aux.coord = coords;

% 3. Visualize the resulting graphs
gplotg(Problem.A, Problem.aux.coord);

% 4. Save the resulting graphs
save('RU_graph.mat', 'Problem')
