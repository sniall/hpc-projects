import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np

rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
rc('text', usetex=True)

data = np.genfromtxt("data.out", delimiter=',', dtype=float, skip_header=1)

num_threads = np.max(data[:,3])

fig = plt.figure(figsize=(5, 5))
fig.add_axes()

ax1 = fig.add_subplot(111)
ax1.set_xlabel("no. of threads")
ax1.set_ylabel("time [s]")
ax1.set_yscale("log")
ax1.set_xticks(np.linspace(0, num_threads, num_threads/4+1))
ax1.grid()

for nx in np.unique(data[:,0]):
  filtered = data[(data[:,0] == nx)]
  ax1.plot(filtered[:,3], filtered[:,4], label="$n_x=" + '{0:g}'.format(float(nx)) + "$", marker='.')

ax1.legend()

plt.savefig('plot_strong.pdf', bbox_inches='tight')

# -------------
data_weak = np.genfromtxt("data_weak.out", delimiter=',', dtype=float, skip_header=1)

fig2 = plt.figure(figsize=(5, 5))
fig2.add_axes()

ax2 = fig2.add_subplot(111)
ax2.set_xlabel("no. of threads")
ax2.set_ylabel("time [s]")
ax2.set_xticks(np.linspace(0, num_threads, num_threads/4+1))
ax2.grid()

ax2.plot(data_weak[:,3], data_weak[:,4], marker='.')

plt.savefig('plot_weak.pdf', bbox_inches='tight')



