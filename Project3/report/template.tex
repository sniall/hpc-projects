\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\input{assignment.sty}
\begin{document}



\setassignment
\setduedate{12 April 2021 (midnight)}

\serieheader{High-Performance Computing Lab for CSE}{2021}{Student: Niall Siegenheim}{Discussed with: Maximilian Herde, Christoph Grötzbach}{Solution for Project 3}{}
\newline

\section*{Methodology}
All numerical experiments are expected to be run on an Euler IV compute node, made up of two 18-core Intel Xeon Gold 6150 CPUs with a base-clock of 2.70 GHz. This allows for the use of the \verb*|AVX-512| instruction set extension for vectorised operations.

% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% ---------------------- Exercise 1 -----------------------------------------%
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Task: Implementing the linear algebra functions and the stencil operators [35 Points]}
The goal of this assignment was to examine the parallelisation of a PDE solver for the so-called \emph{Fisher's equation}:

$$ \frac{\partial s}{\partial t} = D (\frac{\partial^2 s}{\partial x^2} + \frac{\partial^2 s}{\partial y^2}) + Rs(1-s) $$

The implementation uses a finite-difference approach. Each grid-point establishes a non-linear equation solved using Newton's method. In every iteration of Newton's method, a linear system is solved using a matrix-free conjugate gradient solver.

The linear algebra functions implement various arithmetic operations in single methods to supply easy and quick use when doing the calculations to solve the PDE. They are based on \verb*|blas| level 1 reductions (dot product, 2-norm, fill) and \verb*|blas| level 1 vector-vector operations ($ y := \alpha \cdot x + y $, $ y := x + \alpha \cdot (l-r) $, $ y := \alpha \cdot (l-r) $, $ y := \alpha x $, $ y := \alpha \cdot x + \beta \cdot z $ and $ y := x $).

To increase efficiency and make use of our pre-defined hardware, we use Intel \verb*|AVX-512| instructions. These allow us to perform eight operations at the same time using special registers. To do so, we have to include \verb*|immintrin.h| and compile with the option \verb*|-qopenmp| when using the Intel-own \verb*|icc| C++ compiler. This comes at the cost of making the code less readable and restricting use to processors which have the \verb*|AVX-512| instruction set. To handle computations on vectors not perfectly divisible by eight, we also have to use remainder loops using ``naive" instead of intrinsic operations.

Interestingly, the intrinsic \verb*|_mm512_reduce_add_pd| is not included in the \verb*|gcc| implementation of Intel intrinsics. We are therefore forced to use \verb*|icc|, which gives the nice benefit of better optimised ode for our specific processor.

For the implementation of the stencil operators in file \verb*|operators.cpp|, we follow the formulas given in the project description For the interior grid points, the implementation is quite straight-forward. We just have to pay attention to which iteration's values we have to use. On the boundary of the mesh, instead of accessing values $ s $ outside of the mesh, we have to use the (fixed and pre-defined) values of the boundary.

The result after reaching convergence for a grid size of $ 1024^2 $ is depicted in figure \ref{fig:output}.

\begin{figure}
	\centering
	\begin{subfigure}{.45\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{./fig/initial.png}
		\caption{Initial values.}
		\label{fig:initial}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./fig/output.png}
		\caption{Result after reaching convergence.}
		\label{fig:output}
	\end{subfigure}
	\caption{Simulation of Fisher's equation.}
\end{figure}

% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% ---------------------- Exercise 2 -----------------------------------------%
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Task:  Adding OpenMP to the nonlinear PDE mini-app [50 Points]}
To improve performance of our PDE solver, it makes sense to parallelise (i) our linear algebra operations and (ii) our stencil operators.

Parallelising the linear algebra operations is quite straight-forward. As all of them are implemented as \verb*|for|-loops, we can easily use the \verb|omp parallel for| clause for the loops which use intrinsic operations. The remainder loops are still executed by a single thread, as they operate on at most seven vector elements. Numerical experiments show that the overhead of initialising threads outweighs the speed gain of executing at most seven iterations in parallel.

For the implementation of the stencil operations, the whole \verb*|diffusion| method is turned into a \verb|parallel| region. Again, all loops can be parallelised using an \verb*|omp for| clause. The inner grid point calculations scale with $\mathcal{O}(n^2)$, while the boundaries scale with $\mathcal{O}(n)$. The corner calculations are in a \verb|omp single| region, as they are a single calculation and therefore do not have to be computed by more than a single thread.

\subsection*{Benchmarking}
The executable \verb*|main| can be used as in the template to solve the PDE and get the final output from it making use of the maximum number of threads given by the environment variable \verb*|OMP_NUM_THREADS|. The executable \verb*|main_benchmarking| performs both strong and weak scaling benchmarks. 

In the original executable, the parameter $ \alpha $ is set dynamically depending on grid size, integration time and total number of time steps. Since we want to make sure our calculations always converge, we want to fix $ \alpha = 1 $. As the grid size is given when benchmarking, we decide to adjust the total integration time dynamically. Thereby we ensure that the total number of time steps is constant and that we therefore perform the same amount of work compared to the original implementation. This makes sure we can compare computation times between different grid sizes while also knowing our simulations will converge.



\paragraph{Strong scaling}
For strong scaling benchmarking, for every grid size $ n_x \in \{64, 128, 256, 512, 1024\} $ and every number of OpenMP threads $ p \in \{1, \dots, \verb*|OMP_NUM_THREADS|\} $, a solution is calculated and timed $ \verb*|MAX_ITER| = 10 $ times. The median of these timings is then written to an output file.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{./fig/plot_strong.pdf}
	\caption{Strong scaling for parallelised Fisher's equation and 100 time steps.}
	\label{fig:strong}
\end{figure}

The results can be observed in figure \ref{fig:strong}. For small numbers of threads, there is an obvious relationship between larger grid sizes and longer computation times. However, especially for grid sizes 64 and 128 it is obvious that for larger number of threads the trade-off between speed-up by parallelised computations and slow-down by thread overhead is disadvantageous: As for these smaller grids vector sizes are quite small, the overhead of spawning and scheduling threads is bigger than the performance improvement by the few operations they actually perform, quite counter-intuitively leading to worse performance when using a higher number of threads. Also, for large numbers of threads it can actually be more efficient to run a simulation on a finer mesh, which is usually desirable as it makes the solution more accurate. 

For an unknown reason, simulations on the grid size $ n_x = 1024 $ do not converge when run with specific numbers of threads. That is why we observe extreme dips. There seems to be an issue with the stencil operators, but it is not clear what is wrong. Other students' (very similar) implementations lead to divergence as well, but with other numbers of threads. An intrinsic implementation did not have any convergence issues at all for some reason. However, it is clear that this issue is due to the use of OpenMP and is not because of wrong simulation parameters.

A threaded OpenMP PDE solver could produce bitwise-identical results if all arithmetic operations on floating point variables would be executed in the same order as in the serial version. However, this would defeat the purpose of parallelising the code, as there would be no speed-up. Therefore, in a real world it is not possible to produce bitwise-identical results after parallelising the code.

\paragraph{Weak scaling}
To examine weak scaling, we start with a base grid size for $ n_{x,base} = 128 $. Then, for increasing number of threads, we scale the grid to $ n_{x,p} = n_{x,base} \cdot \sqrt{p} $, as grid point iterations scale with $ \mathcal{O}(n_x^2) = \mathcal{O}(n_{x,base}^2 \cdot p) $. Again, every simulation is run $ \verb*|MAX_ITER| = 10 $ times and its median timing is taken and used for evaluation and plotting.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{./fig/plot_weak.pdf}
	\caption{Weak scaling for parallelised Fisher's equation for $ n_{x,base} = 128 $ and $ n_t = 100 $ time steps.}
	\label{fig:weak}
\end{figure}

The benchmark results are depicted in figure \ref{fig:weak}. In an ideal world, the timings would be the same for all numbers of threads, as the amount of necessary computational work is scaled accordingly to the number of processors. However, we cannot observe this in our experiments. For up to three threads, we observe a speed-up probably due to a better trade-off between thread overhead and parallelised computations. However, from seven threads on, the amount of time needed to complete the simulations increases more and more. This can be amounted to limited cache memory and bandwidth limitations when the different threads have to communicate with each other.

\end{document}
