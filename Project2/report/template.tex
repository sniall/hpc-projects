\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{caption}
\captionsetup[figure]{labelsep=space}

\input{assignment.sty}
\begin{document}


\setassignment
\setduedate{26.03.2021 (midnight)}

\serieheader{High-Performance Computing Lab for CSE}{2021}{Student: Niall Siegenheim}{Discussed with: Christoph Grötzbach, Maximilian Herde}{Solution for Project 2}{}
\newline

\section*{Methodology}
All numerical experiments are run on an Euler IV compute node, made up of two 18-core Intel Xeon Gold 6150 CPUs with a base-clock of 2.70 GHz. If not otherwise specified, the baseline for speed-up plots are timings from the original sequential program.

% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% ---------------------- Exercise 1 -----------------------------------------%
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Parallel reduction operations using OpenMP [10 points]}
Dot products are very common in scientific computing. Therefore, it makes sense to think about the efficiency of calculations. The sequential implementation quite simply implements a loop over both vectors, multiplying the corresponding elements and adding them to the total sum. Since these calculations are independent of each other, we can easily parallelise the dot product by splitting the vectors into chunks, calculating the dot product of every chunk using a separate thread and adding everything together at the end.

OpenMP offers two ways to do this. The first method is using a temporary private variable storing each thread's partial sum and later adding all of these together to acquire the total dot product. In an \verb|omp parallel| region, we first apply a \verb|omp for| pragma to our loop and afterwards add the local sum to the total result. This operation has to be marked as \verb|omp critical| to synchronize the total sum between all threads.

The second, more elegant method is to use a \verb*|reduction| clause for \verb*|for| loops. By a simple \verb*|reduction(+:variable)| statement, we inform OpenMP that \verb*|variable| will be updated using add operations in the following loop. Thereby it will make sure to synchronize these calculations between threads.

Interestingly, the \verb*|g++| compiler with optimization flag \verb*|-O3| generates far less efficient machine code for the sequential version opposed to the optimized parallel versions, even though they work very similarly. When running the parallelized code on one thread, both variants are more than three times quicker than the normal sequential code. Therefore, we take the parallelized code on one thread as baseline instead of the sequential code to improve comparability between different number of threads. We will have to assume that the sequential and parallelized code are roughly the same speed when run on a single thread. However, in reality the sequential code will be a little quicker due to less overhead. Assuming the overhead is 20\%, we multiply the timings of the parallelised code on one thread with a factor of 0.8 to get a theoretical estimate of the sequential runtime compiled to the same machine code.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{./img/dot-product/plot1.pdf}
	\caption{-- Performance plots for dot product}
	\label{fig:dp-plot1}
\end{figure}

As seen in figure \ref{fig:dp-plot1}, both variants scale well up to 18 threads. The \verb*|reduction| variant presumably performs a little better due to internal optimization for exactly this kind of use-case. As the number of threads increases, the parallel efficiency slightly decreases. This can be attributed to the overhead of managing the threads, especially the \verb*|critical| statement in one of the variants.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{./img/dot-product/plot2.pdf}
	\caption{-- Stride plot for dot product}
	\label{fig:dp-plot2}
\end{figure}

Interestingly, according to figure \ref{fig:dp-plot2}, on a full 18-core Xeon Gold 6150 CPU, dot products with a size of around $ 10^6 $ elements seem to perform particularly well, while smaller and bigger vector sizes cannot make use of the number of threads as much. For smaller vector sizes, this is probably due to the balance of OpenMP overhead vs. parallelised instructions, while for bigger vector sizes I personally cannot think of an explanation.

\begin{figure}
	
\end{figure}


% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% ---------------------- Exercise 2 -----------------------------------------%
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{The Mandelbrot set using OpenMP [30 points]}

The Mandelbrot set is defined as the set of complex numbers $ c \in \mathbb{C} $, for which the iteration $ z_0 = 0, z_{n+1} = z_n^2 + c $ converges. Using an image whose x-axis corresponds to the real axis and y-axis to the imaginary axis of the complex plane, every pixel represents a distinct complex number whose colour indicates if it converges in the iteration. This yields image \ref{img:mandel}, where the white pixels represent the converging numbers.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{./img/mandel/mandel.png}
	\caption{-- The Mandelbrot set visualised}
	\label{img:mandel}
\end{figure}

To get this image, we iterate for each pixel until we either reach the maximum number of iterations \verb|MAX_ITERS=35207|, defined by ourselves, or $ z_n $ leaves the circle $ \|z_n\| = 2 $. In the first case, $ z_n $ does not diverge for a large number of iterations and we can therefore assume it belongs to the Mandelbrot set. In the second case, $ z_n $ leaves a given bound and we assume it diverges. To calculate the number of floating point operations per second, we also introduce a variable \verb*|nIterationsCount| which counts the total number of iterations. As it is shared between all threads, we have to use an \verb|omp atomic| pragma every time we update it to make sure its value stays synchronised.

Since the convergence can be determined independently for each pixel, the computations can be parallelised easily. In front of the double-loop, looping over every pixel, we use a \verb|omp parallel for| pragma with a \verb|collapse(2)| clause to parallelise both of the loops. We also use \verb|private| and \verb|firstprivate| clauses to control which variables should be a unique copy to every thread.

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{./img/mandel/plot.pdf}
	\caption{}
	\label{fig:mandel_speedup}
\end{figure}

Looking at the speed-up plot in figure \ref{fig:mandel_speedup}, performance scales linearly as expected, but  with a smaller slope of only about $ 0.35 $. This is far from ideal. A reason for this could be the \verb*|png_plot| method, which is called for every pixel and accesses shared data.

% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% ---------------------- Exercise 3 -----------------------------------------%
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Bug hunt [15 points]}

\subsection{Bug 1}
When trying to compile the code given in Bug 1, we get an error that the \verb|omp parallel for| pragma is placed incorrectly.

This is because the \verb|omp parallel for| pragma expects an immediately following \verb*|for| statement. However, in this case, we find an opening curly bracket and an assignment before the actual loop. We can fix this by taking the composed pragma apart. We leave the \verb*|parallel|-part in front of the curly brackets, but move the \verb*|for|-part into the parallel scope right in front of the loop.

\subsection{Bug 2}
When running this code, there appear to be two issues: First of all, all threads output with the same thread id. Secondly, the return value of the summation is incorrect and varies on every execution.

To fix the first issue, we have to append \verb*|private(tid)| to the pragma in line 14 of the code. Since by default all variables in a parallel region are shared, the assignment of \verb|tid| will be overwritten over and over again by every thread. We need to make it private, such that every thread has its own variable storing the id.

For the second issue, we need to analyse what the calculation does to get the correct result. In mathematical notion, it can be written and rewritten as 
$$ \sum_{n=0}^{N} n = \sum_{n=1}^{N} n = \frac{1}{2} N(N+1) $$
Since in our case $ N = 999999 $, the expected result is $ \sum_{n=0}^{999999} n = 4.999995 \cdot 10^{11} $.

In our parallel implementation, the variable \verb*|total| is updated on every iteration. Therefore, it has to be synchronised between all threads. To do so, we add a \verb|reduction(+:total)| clause to the \verb|omp for| pragma.


\subsection{Bug 3}
The execution of this program never terminates when setting \verb|OMP_NUM_THREADS| to any value greater than two. The code uses an \verb|omp sections| clause, which allows the programmer to split the subsequent code into multiple sections. Each section will be executed by a single thread. In our case, both sections call another function called \verb|print_results|, which includes an \verb|omp barrier|. A barrier makes threads stop until all of them have reached it to synchronize execution. However, only two threads out of all reach this barrier due to the sections. Therefore, they will get stuck and never continue execution. A solution to this is simply removing the barrier and possibly moving the subsequent \verb|printf| statement into the previous \verb|critical| region.

\subsection{Bug 4}
Running this code causes a segmentation fault. Looking at the code, every thread has its own instance of the two-dimensional array \verb*|a|, as it is declared as \verb*|private|. It has a size in bytes of $ 1048 \cdot 1048 \cdot 8 \: \mathrm{B} = 8786432 \: \mathrm{B} = 8580.5 \: \mathrm{MB} $. As it is declared as static array, it will be allocated on the program stack. However, the stack size is limited for every program. On Unix shells, the maximum stack size can be printed and adjusted using the command \verb|ulimit -s|. Additionally, we have to make sure the environment variable \verb|OMP_STACKSIZE|, which controls the stack size limit of every additionally spawned thread in OpenMP, is big enough to fit the whole array. By adjusting these two parameters, we make sure the program does not cause any segmentation faults. That means we cannot fix this issue by adjusting the code, but by changing the environment's limits it is running in.

\subsection{Bug 5}
Similar as in Bug 3, the program never terminates. The reason for this is a dead-lock between two threads. Once again, a \verb|pragma omp sections| clause with two sections is used. In the first section, an array \verb|a| is initialized and added to an array \verb*|b|, while in the second section array \verb*|b| is initialized with different values and added to array \verb*|a|. The implementation uses two locks, \verb*|locka| and \verb*|lockb|. The first section locks \verb*|locka| on initialization of the array and locks \verb*|lockb| at the start of the addition, while the second section does it vice-versa. Because there is a barrier before the start of the sections, both threads will start working on them at the same time. That means after the initialization loops both threads will try to acquire each other's already set locks, which will never be possible. Therefore, we end up with a dead-lock.


\section{Parallel histogram calculation using OpenMP [15 points]}
This task focuses on calculating a histogram of a set of integers stored in an input array in parallel. In the original sequential implementation, we simply loop all over all elements in the input array and increment the corresponding bin in the histogram array for each element.

The algorithm can be parallelised by segregating the input array into chunks and letting different threads work on a chunk each. However, in a naive implementation, the histogram array would be shared between all threads and every bin update would have to be an atomic operation. This would make the algorithm very inefficient. Therefore, we propose an alternative: Every thread has its own, private histogram array it updates on its own chunk. Then, once it is done, this array is added to the global histogram array using atomic operations. Since the number of bins is a lot smaller than the size of the input array, the number of atomic, potentially slow, operations is a lot smaller. For the implementation we use the already known \verb*|omp| clauses \verb*|parallel|, \verb*|for| and \verb*|atomic|.

\begin{figure}
	\centering
	\includegraphics{./img/hist/plot.pdf}
	\caption{}
	\label{fig:hist_plot}
\end{figure}

In speed-up plot \ref{fig:hist_plot}, the algorithm scales well up to about twelve threads. With a higher number of cores, the speed-up slows down. The dip from 18 to 19 threads can easily be explained by looking at Euler IV's architecture: A node is made up of two 18-Core Intel Xeon Gold 6150 processors. Therefore, using more than 18 threads will mean that data between two different chips has to be synchronised, leading to a hit on performance. The jump from 27 to 28 cores is more interesting, as the jump does not rebound. Performance stays consistently better with more than 27 cores. The reason for this is not obvious to me. Possibly, the hardware architecture has technology which speeds up synchronisation between two CPUs when using a high number of cores or the scheduler is able to optimise operations.


% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% ---------------------- Exercise 5 -----------------------------------------%
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Parallel loop dependencies with OpenMP [15 points]}
In some algorithms, a loop's $n+1$ st iteration depends on the result obtained in the $n$ th iteration. In this particular case, we want to calculate $ S_n = S_0 \cdot u^n, \ \ n = 0, \dots, N $ efficiently, where $ S_n $, $ u $ and $ n $ are given. We do so by simply setting the first element in the array to $ S_0 $ and subsequently looping $ S_{n+1} = S_{n} \cdot u, \ \ n = 1, \dots, N $. We once again can segregate this loop into chunks and let different threads work on each chunk. However, the chunks are not independent from each other. To combat this, we track what the previous iteration $ n_\mathrm{last} $ was for every thread. If $ n-1 = n_\mathrm{last} $ for some thread, we can simply use $ S_{n-1} $ for iteration $ S_{n} $. Otherwise, we cannot be sure whether this value is ready yet and therefore calculate it using the power function. This can be mathematically formulated as $ S_n = S_{n_\mathrm{last}} \cdot u^{n - n_\mathrm{last}} $. We have to be careful here with indexing and the definition of our variables in code. This implementation works with all schedulers, as a thread can keep track of (seemingly) erratic jumping around in the lexicographic order of the loop using the variable $n_\mathrm{last}$.

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{./img/loop-dependencies/plot.pdf}
	\caption{}
	\label{fig:rec_plot}
\end{figure}

Speed-up plot \ref{fig:rec_plot} shows good scaling behaviour up to seven threads using the \verb*|static| scheduler. From there on up to 30 threads the performance stagnates at a speed-up factor of six to seven, which is far from optimal. This might be due to the too many calls to the \verb*|pow| function. Generally, to optimise the speed gain using this algorithm the number of threads has to be balanced between too few, not parallelising operations that could be parallelised, and too many, resulting in many calls to the inefficient \verb*|pow| function. Curiously, once again a significant sudden performance difference is visible at more than 30 threads, either due to hardware optimization or scheduling.

A speed difference between using the \emph{relative} \verb*|pow| call described earlier, taking $ S_{n_\mathrm{last}} $ and computing $ S_n $ from it using power laws, or an \emph{absolute} \verb*|pow| call, using $ S_0 $ and calculating $ S_n $ freshly, could not be observed in experiments.


\end{document}
