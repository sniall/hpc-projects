#include <stdio.h>
#include <stdlib.h>

#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <math.h>


#include "consts.h"
#include "pngwriter.h"
#include "omp.h"

unsigned long get_time() {
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return tp.tv_sec * 1000000 + tp.tv_usec;
}

int main(int argc, char **argv) {
  FILE *fp;
  fp = fopen("data.out", "w+");
  
  png_data *pPng = png_create(IMAGE_WIDTH, IMAGE_HEIGHT);

  const int max_p = omp_get_max_threads();
  
  fprintf(fp, "threads,total_wallclock,total_num_it,avg_time_pxl,avg_time_it,it_per_sec,mflops\n");
  
  for (int p = 1; p <= max_p; ++p) {
    printf("p=%d, max_p=%d\n", p, max_p);
    
    double x, y, x2, y2, cx, cy;

    double fDeltaX = (MAX_X - MIN_X) / (double)IMAGE_WIDTH;
    double fDeltaY = (MAX_Y - MIN_Y) / (double)IMAGE_HEIGHT;

    long nTotalIterationsCount = 0;
    unsigned long nTimeStart = get_time();

    long i, j, n;

    n = 0;
    // do the calculation
    #pragma omp parallel for collapse(2) firstprivate(n) \
       private(i, j, x, y, x2, y2, cy, cx) num_threads(p)
    for (j = 0; j < IMAGE_HEIGHT; j++) {
      for (i = 0; i < IMAGE_WIDTH; i++) {
        cx = i * fDeltaX + MIN_X;
        cy = j * fDeltaY + MIN_Y;
        
        x = cx;
        y = cy;
        x2 = x * x;
        y2 = y * y;
        
        // compute the orbit z, f(z), f^2(z), f^3(z), ...
        // count the iterations until the orbit leaves the circle |z|=2.
        // stop if the number of iterations exceeds the bound MAX_ITERS.
        for (n = 1; n < MAX_ITERS && x2+y2 < 4; n++) {
          double temp = x2 - y2 + cx;
          y = 2*x*y + cy;
          x = temp;
          
          x2 = x * x;
          y2 = y * y;
        }
        
        #pragma omp atomic
        nTotalIterationsCount += n;

        // n indicates if the point belongs to the mandelbrot set
        // plot the number of iterations at point (i, j)
        int c = ((long)n * 255) / MAX_ITERS;
        png_plot(pPng, i, j, c, c, c);
      }
    }
    unsigned long nTimeEnd = get_time();
           
    fprintf(fp, "%d,%e,%d,%e,%e,%e,%e\n", p,
           (nTimeEnd - nTimeStart) / 1000.0,
           nTotalIterationsCount, 
           (nTimeEnd - nTimeStart) / (double)(IMAGE_WIDTH * IMAGE_HEIGHT),
           (nTimeEnd - nTimeStart) / (double)nTotalIterationsCount,
           nTotalIterationsCount / (double)(nTimeEnd - nTimeStart) * 1e6,
           nTotalIterationsCount * 8.0 / (double)(nTimeEnd - nTimeStart));
  }
  
  png_write(pPng, "mandel.png");
  fclose(fp);
  return 0;
}
