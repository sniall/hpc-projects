import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np

rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
rc('text', usetex=True)

data = np.genfromtxt("data.out", delimiter=',', dtype=float, skip_header=1)
n = len(data[:,0])

timing_seq = 454086; 

fig = plt.figure(figsize=(6, 4))
fig.add_axes()

# Speed-up plot for matrix size n
ax1 = fig.add_subplot(111)
ax1.set_title("Speed-up plot for Mandelbrot set at resolution 4096x4096")
ax1.set_xlabel("no. of threads $p$")
ax1.set_ylabel("speedup")
ax1.grid()

ax1.plot(np.linspace(1,n,n*100), np.linspace(1,n,n*100), label="ideal speedup", alpha=0.5)
ax1.plot(data[:,0], timing_seq/data[:,1], marker='o', label="implementation")
ax1.legend()

plt.savefig('plot.pdf', bbox_inches='tight')
