#include "walltime.h"
#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <algorithm>
#include <omp.h>

#define VEC_SIZE 1000000000
#define BINS 16
#define ITERATIONS 100

using namespace std;

int main() {
  // Initialize random number generator
  unsigned int seed = 123;
  float mean = BINS / 2.0;
  float sigma = BINS / 12.0;
  std::default_random_engine generator(seed);
  std::normal_distribution<float> distribution(mean, sigma);

  // Generate random sequence
  // Note: normal distribution is on interval [-inf; inf]
  //       we want [0; BINS-1]
  int *vec = new int[VEC_SIZE];
  for (long i = 0; i < VEC_SIZE; ++i) {
    vec[i] = int(distribution(generator));
    if (vec[i] < 0)
      vec[i] = 0;
    if (vec[i] > BINS - 1)
      vec[i] = BINS - 1;
  }
  
  std::ofstream file;
  file.open("data.out", std::ios::trunc);
  file << "p,time\n";

  for (int p = 1; p <= omp_get_max_threads(); ++p) {
  
    std::vector<double> timings;
    for (int iter = 0; iter < ITERATIONS; ++iter) {
      double time_start, time_end;

      // Initialize histogram
      // Set all bins to zero
      long dist[BINS];
      for (int i = 0; i < BINS; ++i) {
        dist[i] = 0;
      }

      time_start = wall_time();

      #pragma omp parallel num_threads(p)
      {
        long dist_local[BINS];
        for (int i = 0; i < BINS; ++i) {
          dist_local[i] = 0;
        }
        
        #pragma omp for
        for (int i = 0; i < VEC_SIZE; ++i) {
          dist_local[vec[i]]++;
        }
        
        for (int i = 0; i < BINS; ++i) {
          #pragma omp atomic
          dist[i] += dist_local[i];
        }
      }
      
      time_end = wall_time();
      timings.push_back(time_end - time_start);
    }
    
    std::sort(timings.begin(), timings.end());
    
    file << p << "," << timings.at(ITERATIONS / 2) << endl;
  }
  
  file.close();

  delete[] vec;

  return 0;
}
