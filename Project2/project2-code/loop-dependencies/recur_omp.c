#include "walltime.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define REFSOL 485165097.62511122
#define ITER 30

int compare_double(const void * a, const void * b)
{
  if ( *(double*)a <  *(double*)b ) return -1;
  if ( *(double*)a == *(double*)b ) return 0;
  if ( *(double*)a >  *(double*)b ) return 1;
}

int main(int argc, char *argv[]) {
  int N = 2000000000;
  double Sn;
  double up = 1.00000001;
  
  /* allocate memory for the recursion */
  double *opt = (double *)malloc((N + 1) * sizeof(double));

  if (opt == NULL)
    die("failed to allocate problem size");

  FILE *fp;
  fp = fopen("data.out", "w+");
  fprintf(fp, "threads,runtime,error\n");
  
  int p;
  for (p = 1; p <= omp_get_max_threads(); ++p) {
    double timings[ITER];
    
    int iter;
    for (iter = 0; iter < ITER; ++iter) {
      Sn = 1.00000001;
      int last_n = -1;
      
      double time_start = wall_time();
      #pragma omp parallel num_threads(p)
      {
        double Sn_start = Sn;
        int n;
        
        #pragma omp for firstprivate(Sn, last_n) schedule(dynamic)
        for (n = 0; n <= N; ++n) {
          if (last_n != n-1) {
            Sn = Sn * pow(up, n-last_n-1);
          }
          
          opt[n] = Sn;
          Sn *= up;
          last_n = n;
        }
      }
      
      Sn = opt[N] * up;
      
      double time_end = wall_time();
      timings[iter] = time_end - time_start;
    }
    
    qsort(timings, ITER, sizeof(double), compare_double);
    
    printf("Num Threads:       :  %d\n", p);
    printf("Median RunTime     :  %f seconds\n", timings[ITER/2]);
    printf("Final Result Sn    :  %.17g \n", Sn);
    printf("Error        Sn    :  %e \n", fabs(Sn-REFSOL));
    
    fprintf(fp, "%d,%e,%e\n", p, timings[ITER/2], fabs(Sn-REFSOL));
  }

  return 0;
}
