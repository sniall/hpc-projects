#include "walltime.h"
#include <math.h>

#define ITER 30

int compare_double(const void * a, const void * b)
{
  if ( *(double*)a <  *(double*)b ) return -1;
  if ( *(double*)a == *(double*)b ) return 0;
  if ( *(double*)a >  *(double*)b ) return 1;
}

int main(int argc, char *argv[]) {
  int N = 2000000000;
  double up = 1.00000001;
  double Sn = 1.00000001;
  int n;
  /* allocate memory for the recursion */
  double *opt = (double *)malloc((N + 1) * sizeof(double));

  if (opt == NULL)
    die("failed to allocate problem size");

  double timings[ITER];
  
  int iter;
  for (iter = 0; iter < ITER; iter++) {
    Sn = 1.00000001;
    
    double time_start = wall_time();
    
    for (n = 0; n <= N; ++n) {
      opt[n] = Sn;
      Sn *= up;
    }
    
    double time_end = wall_time();
    timings[iter] = time_end - time_start;
  }
  
  qsort(timings, ITER, sizeof(double), compare_double);
  
  printf("Sequential RunTime :  %f seconds\n", timings[ITER/2]);
  printf("Final Result Sn    :  %.17g \n", Sn);

  double temp = 0.0;
  for (n = 0; n <= N; ++n) {
    temp += opt[n] * opt[n];
  }
  printf("Result ||opt||^2_2 :  %f\n", temp / (double)N);
  printf("\n");

  return 0;
}
