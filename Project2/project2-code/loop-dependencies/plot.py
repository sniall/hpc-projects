import matplotlib.pyplot as plt
from matplotlib import rc
import sys
import numpy as np

rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
rc('text', usetex=True)

data = np.genfromtxt("data.out", delimiter=',', dtype=float, skip_header=1)
n = len(data[:,1]);

# extracted as median from experiments
timing_seq = 2.848210;

fig = plt.figure(figsize=(6, 4))
fig.add_axes()

ax1 = fig.add_subplot(111)
ax1.set_title("Speed-up plot for loop dependencies")
ax1.set_xlabel("no. of threads $p$")
ax1.set_ylabel("factor of speedup")
ax1.grid()

ax1.plot(np.linspace(1,n,n*100), np.linspace(1,n,n*100), label="ideal speedup", alpha=0.5)
ax1.plot(data[:,0], timing_seq/data[:,1], marker='o', label="implementation")
ax1.legend()

plt.savefig('plot.pdf', bbox_inches='tight')
