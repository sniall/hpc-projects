import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np

rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
rc('text', usetex=True)

data = np.genfromtxt("data.out", delimiter=',', dtype=float, skip_header=1)

num_threads = np.max(data[:,0])

fig = plt.figure(figsize=(10, 4))
fig.add_axes()

# Speed-up plot for matrix size n
n = 1e6
ax1 = fig.add_subplot(121)
ax1.set_title("Speed-up plot for vector size $ n=" + '{:.0e}'.format(n) + "$")
ax1.set_xlabel("no. of threads")
ax1.set_ylabel("speed-up factor")
ax1.set_xticks([0, 5, 10, 15])
ax1.grid()

filtered = data[(data[:,1] == n)]
ax1.plot(np.linspace(1,num_threads,num_threads*100), np.linspace(1,num_threads,num_threads*100), label="ideal speedup", alpha=0.5)
ax1.plot(filtered[:,0], 0.8 * filtered[0,3]/filtered[:,3], marker='o', label='OpenMP reduction')
ax1.plot(filtered[:,0], 0.8 * filtered[0,4]/filtered[:,4], marker='o', label='OpenMP critical')
ax1.legend()

# Parallel efficiency
n = 1e6
ax1 = fig.add_subplot(122)
ax1.set_title("Parallel efficiency plot for vector size $ n=" + '{:.0e}'.format(n) + "$")
ax1.set_xlabel("no. of threads")
ax1.set_ylabel("parallel efficiency")
ax1.set_xticks([0, 5, 10, 15])
ax1.grid()

filtered = data[(data[:,1] == n)]
ax1.plot(filtered[:,0], 0.8 * filtered[0,3]/filtered[:,3]/filtered[:,0], marker='o', label='OpenMP reduction')
ax1.plot(filtered[:,0], 0.8 * filtered[0,4]/filtered[:,4]/filtered[:,0], marker='o', label='OpenMP critical')
ax1.legend()

plt.savefig('plot1.pdf', bbox_inches='tight')

# Vector size plot on p cores
p = 18
fig2 = plt.figure(figsize=(5,5))
ax2 = fig2.add_subplot(111)
ax2.set_title("Stride plot for " + str(p) + " threads")
ax2.set_xscale("log")
ax2.set_xlabel("vector size $ n $")
ax2.set_ylabel("speed-up factor")
ax2.set_ylim([0,np.floor(p*1.2)])
ax2.grid()

filtered_seq = data[(data[:,0] == 1)]
filtered = data[(data[:,0] == p)]
#ax2.plot(filtered[:,1], filtered[:,2], marker='o', label='Naive')
ax2.plot(filtered[:,1], 0.8 * filtered_seq[:,3]/filtered[:,3], marker='o', label='OpenMP reduction')
ax2.plot(filtered[:,1], 0.8 * filtered_seq[:,4]/filtered[:,4], marker='o', label='OpenMP critical')
ax2.legend()

plt.savefig('plot2.pdf', bbox_inches='tight')


