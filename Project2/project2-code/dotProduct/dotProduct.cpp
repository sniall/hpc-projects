// #include <omp.h>
#include "walltime.h"
#include <iostream>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <omp.h>

#define NUM_ITERATIONS 100
#define EPSILON 0.1

using namespace std;

int main() {
  std::ofstream file;
  file.open("data.out", std::ios::trunc);

  const int start_N = 100000;
  const int max_p = omp_get_max_threads();
  file << "p,N,serial,reduction,critical" << std::endl;
  
  for (int p = 1; p <= max_p; ++p) {
    int N = start_N;
    
    for (int i = 1; i <= 5; ++i, N *= 10) {
      double time_serial, time_start = 0.0;
      double *a, *b;

      // Allocate memory for the vectors as 1-D arrays
      a = new double[N];
      b = new double[N];

      // Initialize the vectors with some values
      for (int i = 0; i < N; i++) {
        a[i] = i;
        b[i] = i / 10.0;
      }

      long double alpha = 0;
      // serial execution
      // Note that we do extra iterations to reduce relative timing overhead
      time_start = wall_time();
      for (int iterations = 0; iterations < NUM_ITERATIONS; iterations++) {
        alpha = 0.0;
        for (int i = 0; i < N; i++) {
          alpha += a[i] * b[i];
        }
      }
      
      time_serial = wall_time() - time_start;
      

      long double alpha_parallel = 0;
      double time_red = 0;
      double time_critical = 0;

      //   i.  Using reduction pragma
      time_start = wall_time();
      for (int iterations = 0; iterations < NUM_ITERATIONS; iterations++) {
        alpha_parallel = 0.0;
        #pragma omp parallel for reduction(+:alpha_parallel) num_threads(p)
        for (int i = 0; i < N; i++) {
          alpha_parallel += a[i] * b[i];
        }
      }
      time_red = wall_time() - time_start;
      
      //   ii. Using  critical pragma
      time_start = wall_time();
      for (int iterations = 0; iterations < NUM_ITERATIONS; iterations++) {
        alpha_parallel = 0.0;
        #pragma omp parallel shared(alpha_parallel) num_threads(p)
        {
          double alpha_local = 0.0;
          
          #pragma omp for
          for (int i = 0; i < N; i++) {
            alpha_local += a[i] * b[i];
          }
          
          #pragma omp critical
          alpha_parallel += alpha_local;
        }
        
      }
      time_critical = wall_time() - time_start;

      if ((fabs(alpha_parallel - alpha) / fabs(alpha_parallel)) > EPSILON) {
        cout << "parallel reduction: " << alpha_parallel << ", serial: " << alpha
             << "\n";
        cerr << "Alpha not yet implemented correctly!\n";
      }
      
      file << p << "," << N << "," << time_serial << "," << time_red << "," << time_critical << std::endl;

      // De-allocate memory
      delete[] a;
      delete[] b;
    }
  }
  
  file.close();
  return 0;
}
