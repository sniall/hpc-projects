#include "data.h"

#include <mpi.h>
#include <omp.h>

#include <cmath>
#include <iostream>

namespace data {

// fields that hold the solution
Field y_new;
Field y_old;

// fields that hold the boundary points
Field bndN;
Field bndE;
Field bndS;
Field bndW;

// buffers used during boundary halo communication
Field buffN;
Field buffE;
Field buffS;
Field buffW;

Discretization options;
SubDomain domain;

void SubDomain::init(int mpi_rank, int mpi_size,
                     Discretization& discretization) {
  int dims[2] = {0, 0};
  // TODO determine the number of subdomains in the x and y dimensions using
  // "MPI_Dims_create"
  int err = MPI_Dims_create(mpi_size, 2, dims);
  assert(err == MPI_SUCCESS && "Dims_create failed.");

  ndomy = dims[0];
  ndomx = dims[1];

  int periods[2] = {0, 0};
  // TODO create a 2D non-periodic cartesian topology using "MPI_Cart_create"
  err = MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 1, &comm_cart);
  assert(err == MPI_SUCCESS && "Cart_create failed.");
  err = MPI_Comm_rank(comm_cart, &rank);
  assert(err == MPI_SUCCESS && "Comm_rank failed.");
  err = MPI_Comm_size(comm_cart, &size);
  assert(err == MPI_SUCCESS && "Comm_size failed.");

  int coords[2];
  // TODO retrieve coordinates of the rank in the topology using
  // "MPI_Cart_coords"
  err = MPI_Cart_coords(comm_cart, rank, 2, coords);
  assert(err == MPI_SUCCESS && "Card_coords failed.");

  domy = coords[0] + 1;
  domx = coords[1] + 1;

  // TODO set neighbours for all directions using "MPI_Cart_shift"
  // i.e. set neighbour_south neighbour_north neighbour_east neighbour_west
  err = MPI_Cart_shift(comm_cart, 0, 1, &neighbour_south, &neighbour_north);
  assert(err == MPI_SUCCESS && "Cart_shift failed.");
  err = MPI_Cart_shift(comm_cart, 1, 1, &neighbour_west, &neighbour_east);
  assert(err == MPI_SUCCESS && "Cart_shift failed.");

  // get bounding box
  nx = discretization.nx / ndomx;
  ny = discretization.ny / ndomy;
  startx = (domx - 1) * nx + 1;
  starty = (domy - 1) * ny + 1;

  // adjust for grid dimensions that do not divided evenly between the
  // sub-domains
  if (domx == ndomx) nx = discretization.nx - startx + 1;
  if (domy == ndomy) ny = discretization.ny - starty + 1;

  endx = startx + nx - 1;
  endy = starty + ny - 1;

  // get total number of grid points in this sub-domain
  N = nx * ny;
}

// print domain decomposition information to stdout
void SubDomain::print() {
  for (int i = 0; i < size; i++) {
    if (rank == i) {
      std::cout << "rank " << rank << "/" << size << " : (" << domx << ","
                << domy << ")"
                << " neigh N:S " << neighbour_north << ":" << neighbour_south
                << " neigh E:W " << neighbour_east << ":" << neighbour_west
                << " local dims " << nx << " x " << ny << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
  double time = omp_get_wtime();
  // add artificial pause so that output doesn't pollute later output
  while (omp_get_wtime() - time < 1e-1)
    ;
}

}  // namespace data
