//******************************************
// operators.cpp
// based on min-app code written by Oliver Fuhrer, MeteoSwiss
// modified by Ben Cumming, CSCS
// *****************************************

// Description: Contains simple operators which can be used on 3d-meshes

#include "operators.h"

#include <emmintrin.h>
#include <immintrin.h>
#include <mpi.h>

#include <iostream>

#ifdef BLAS
#include <mkl.h>
#endif

#include "data.h"
#include "linalg.h"
#include "stats.h"

namespace operators {

// input: s, gives updated solution for f
// only handles interior grid points, as boundary points are fixed
// those inner grid points neighbouring a boundary point, will in the following
// be referred to as boundary points and only those grid points
// only neighbouring non-boundary points are called inner grid points
void diffusion(const data::Field &s, data::Field &f) {
  using data::domain;
  using data::options;

  using data::bndE;
  using data::bndN;
  using data::bndS;
  using data::bndW;

  using data::buffE;
  using data::buffN;
  using data::buffS;
  using data::buffW;

  using data::y_old;

  double alpha = options.alpha;
  double beta = options.beta;

  int nx = domain.nx;
  int ny = domain.ny;
  int iend = nx - 1;
  int jend = ny - 1;

  int err;
  MPI_Request requests_send[4], requests_recv[4];

  // TODO exchange the ghost cells
  // try overlapping computation and communication
  // by using  MPI_Irecv and MPI_Isend.
  if (domain.neighbour_north >= 0) {
#ifndef BLAS
    for (std::size_t i = 0; i < nx; ++i) {
      buffN[i] = s(i, ny - 1);
    }
#else
    cblas_dcopy(nx, &s[(ny - 1) * nx], 1, &buffN[0], 1);
#endif

    err = MPI_Isend(buffN.data(), nx, MPI_DOUBLE, domain.neighbour_north,
                    domain.rank, domain.comm_cart, &requests_send[0]);
    assert(err == MPI_SUCCESS);
    err = MPI_Irecv(bndN.data(), nx, MPI_DOUBLE, domain.neighbour_north,
                    MPI_ANY_TAG, domain.comm_cart, &requests_recv[0]);
    assert(err == MPI_SUCCESS);
  }

  if (domain.neighbour_south >= 0) {
#ifndef BLAS
    for (std::size_t i = 0; i < nx; ++i) {
      buffS[i] = s(i, 0);
    }
#else
    cblas_dcopy(nx, &s[0], 1, &buffS[0], 1);
#endif

    err = MPI_Isend(buffS.data(), nx, MPI_DOUBLE, domain.neighbour_south,
                    domain.rank, domain.comm_cart, &requests_send[1]);
    assert(err == MPI_SUCCESS);
    err = MPI_Irecv(bndS.data(), nx, MPI_DOUBLE, domain.neighbour_south,
                    MPI_ANY_TAG, domain.comm_cart, &requests_recv[1]);
    assert(err == MPI_SUCCESS);
  }
  if (domain.neighbour_east >= 0) {
#ifndef BLAS
    for (std::size_t i = 0; i < ny; ++i) {
      buffE[i] = s(nx - 1, i);
    }
#else
    cblas_dcopy(ny, &s[nx - 1], nx, &buffE[0], 1);
#endif

    err = MPI_Isend(buffE.data(), ny, MPI_DOUBLE, domain.neighbour_east,
                    domain.rank, domain.comm_cart, &requests_send[2]);
    assert(err == MPI_SUCCESS);
    err = MPI_Irecv(bndE.data(), ny, MPI_DOUBLE, domain.neighbour_east,
                    MPI_ANY_TAG, domain.comm_cart, &requests_recv[2]);
    assert(err == MPI_SUCCESS);
  }
  if (domain.neighbour_west >= 0) {
#ifndef BLAS
    for (std::size_t i = 0; i < ny; ++i) {
      buffW[i] = s(0, i);
    }
#else
    cblas_dcopy(ny, &s[0], nx, &buffW[0], 1);
#endif

    err = MPI_Isend(buffW.data(), ny, MPI_DOUBLE, domain.neighbour_west,
                    domain.rank, domain.comm_cart, &requests_send[3]);
    assert(err == MPI_SUCCESS);
    err = MPI_Irecv(bndW.data(), ny, MPI_DOUBLE, domain.neighbour_west,
                    MPI_ANY_TAG, domain.comm_cart, &requests_recv[3]);
    assert(err == MPI_SUCCESS);
  }

#ifndef AVX_512

  // the interior grid points
  for (int j = 1; j < jend; j++) {
    for (int i = 1; i < iend; i++) {
      f(i, j) = -(4. + alpha) * s(i, j) + s(i - 1, j) + s(i + 1, j) +
                s(i, j - 1) + s(i, j + 1) + beta * s(i, j) * (1. - s(i, j)) +
                alpha * y_old(i, j);
    }
  }

#else

  const int upper_aligned_iend = iend - (iend - 1) % 8;
  const int xdim = f.xdim();
  const double alpha4 = -(4. + alpha);
  __m128d alpha4_128 = _mm_load_sd(&alpha4);
  __m512d alpha4_512 = _mm512_broadcastsd_pd(alpha4_128);
  __m128d alpha_128 = _mm_load_sd(&alpha);
  __m512d alpha_512 = _mm512_broadcastsd_pd(alpha_128);
  __m128d beta_128 = _mm_load_sd(&beta);
  __m512d beta_512 = _mm512_broadcastsd_pd(beta_128);
  constexpr double one = 1;
  __m128d one_128 = _mm_load_sd(&one);
  __m512d one_512 = _mm512_broadcastsd_pd(one_128);

  __m512d fij, sij;

  // the interior grid points
  for (int j = 1; j < jend; ++j) {
    for (int i = 1; i < upper_aligned_iend; i += 8) {
      fij = _mm512_loadu_pd(&f[i + j * xdim]);

      sij = _mm512_loadu_pd(&s[i + j * xdim]);
      fij = _mm512_fmadd_pd(
          alpha4_512, sij,
          _mm512_mul_pd(sij,
                        _mm512_mul_pd(beta_512, _mm512_sub_pd(one_512, sij))));
      sij = _mm512_loadu_pd(&s[i - 1 + j * xdim]);
      fij = _mm512_add_pd(sij, fij);
      sij = _mm512_loadu_pd(&s[i + 1 + j * xdim]);
      fij = _mm512_add_pd(sij, fij);
      sij = _mm512_loadu_pd(&s[i + (j - 1) * xdim]);
      fij = _mm512_add_pd(sij, fij);
      sij = _mm512_loadu_pd(&s[i + (j + 1) * xdim]);
      fij = _mm512_add_pd(sij, fij);
      sij = _mm512_loadu_pd(&y_old[i + j * xdim]);
      fij = _mm512_fmadd_pd(alpha_512, sij, fij);

      _mm512_storeu_pd(&f[i + j * xdim], fij);
    }
  }

  for (int j = 1; j < jend; ++j) {
    for (int i = upper_aligned_iend; i < iend; ++i) {
      f(i, j) = -(4. + alpha) * s(i, j) + s(i - 1, j) + s(i + 1, j) +
                s(i, j - 1) + s(i, j + 1) + beta * s(i, j) * (1. - s(i, j)) +
                alpha * y_old(i, j);
    }
  }

#endif

  // TODO wait on the receives from the outstanding MPI_Irecv using MPI_Waitall.
  if (domain.neighbour_east >= 0) {
    err = MPI_Wait(&requests_send[2], MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
    err = MPI_Wait(&requests_recv[2], MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
  }

  // the east boundary
  {
    int i = nx - 1;
    for (int j = 1; j < jend; j++) {
      f(i, j) = -(4. + alpha) * s(i, j) + s(i - 1, j) + s(i, j - 1) +
                s(i, j + 1) + alpha * y_old(i, j) + bndE[j] +
                beta * s(i, j) * (1.0 - s(i, j));
    }
  }

  if (domain.neighbour_west >= 0) {
    err = MPI_Wait(&requests_send[3], MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
    err = MPI_Wait(&requests_recv[3], MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
  }

  // the west boundary
  {
    int i = 0;
    for (int j = 1; j < jend; j++) {
      f(i, j) = -(4. + alpha) * s(i, j) + s(i + 1, j) + s(i, j - 1) +
                s(i, j + 1) + alpha * y_old(i, j) + bndW[j] +
                beta * s(i, j) * (1.0 - s(i, j));
    }
  }

  if (domain.neighbour_north >= 0) {
    err = MPI_Wait(&requests_send[0], MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
    err = MPI_Wait(&requests_recv[0], MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
  }

  // the north boundary (plus NE and NW corners)
  {
    int j = ny - 1;

    {
      int i = 0;  // NW corner
      f(i, j) = -(4. + alpha) * s(i, j) + s(i + 1, j) + s(i, j - 1) +
                alpha * y_old(i, j) + bndW[j] + bndN[i] +
                beta * s(i, j) * (1.0 - s(i, j));
    }

    // north boundary
    for (int i = 1; i < iend; i++) {
      f(i, j) = -(4. + alpha) * s(i, j) + s(i - 1, j) + s(i + 1, j) +
                s(i, j - 1) + alpha * y_old(i, j) + bndN[i] +
                beta * s(i, j) * (1.0 - s(i, j));
    }

    {
      int i = nx - 1;  // NE corner
      f(i, j) = -(4. + alpha) * s(i, j) + s(i - 1, j) + s(i, j - 1) +
                alpha * y_old(i, j) + bndE[j] + bndN[i] +
                beta * s(i, j) * (1.0 - s(i, j));
    }
  }

  if (domain.neighbour_south >= 0) {
    err = MPI_Wait(&requests_send[1], MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
    err = MPI_Wait(&requests_recv[1], MPI_STATUS_IGNORE);
    assert(err == MPI_SUCCESS);
  }

  // the south boundary
  {
    int j = 0;

    {
      int i = 0;  // SW corner
      f(i, j) = -(4. + alpha) * s(i, j) + s(i + 1, j) + s(i, j + 1) +
                alpha * y_old(i, j) + bndW[j] + bndS[i] +
                beta * s(i, j) * (1.0 - s(i, j));
    }

    // south boundary
    for (int i = 1; i < iend; i++) {
      f(i, j) = -(4. + alpha) * s(i, j) + s(i - 1, j) + s(i + 1, j) +
                s(i, j + 1) + alpha * y_old(i, j) + bndS[i] +
                beta * s(i, j) * (1.0 - s(i, j));
    }

    {
      int i = nx - 1;  // SE corner
      f(i, j) = -(4. + alpha) * s(i, j) + s(i - 1, j) + s(i, j + 1) +
                alpha * y_old(i, j) + bndE[j] + bndS[i] +
                beta * s(i, j) * (1.0 - s(i, j));
    }
  }

  // Accumulate the flop counts
  // 8 ops total per point
  stats::flops_diff += +12 * (nx - 2) * (ny - 2)  // interior points
                       + 11 * (nx - 2 + ny - 2)   // NESW boundary points
                       + 11 * 4;                  // corner points
}

}  // namespace operators
