// linear algebra subroutines
// Ben Cumming @ CSCS

#include <emmintrin.h>
#include <immintrin.h>

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>

#ifdef BLAS
#include <mkl.h>
#endif

#include <mpi.h>

#include "data.h"
#include "linalg.h"
#include "operators.h"
#include "stats.h"

namespace linalg {

bool cg_initialized = false;
Field r;
Field Ap;
Field p;
Field Fx;
Field Fxold;
Field v;
Field xold;

using namespace operators;
using namespace stats;
using data::Field;

// initialize temporary storage fields used by the cg solver
// I do this here so that the fields are persistent between calls
// to the CG solver. This is useful if we want to avoid malloc/free calls
// on the device for the OpenACC implementation (feel free to suggest a better
// method for doing this)
void cg_init(int nx, int ny) {
  Ap.init(nx, ny);
  r.init(nx, ny);
  p.init(nx, ny);
  Fx.init(nx, ny);
  Fxold.init(nx, ny);
  v.init(nx, ny);
  xold.init(nx, ny);

  cg_initialized = true;
}

////////////////////////////////////////////////////////////////////////////////
//  blas level 1 reductions
////////////////////////////////////////////////////////////////////////////////

// computes the inner product of x and y
// x and y are vectors on length N
double hpc_dot(Field const& x, Field const& y) {
  double result = 0;
  double result_global = 0;
  int N = y.length();
#if !defined(AVX_512) || !defined(BLAS)
  for (int i = 0; i < N; i++) result += x[i] * y[i];
#elif defined(BLAS)
  result = cblas_ddot(N, x.data(), 1, y.data(), 1);
#else
  const int upper_aligned_N = N - N % 8;
  __m512d x512, y512;

  for (int i = 0; i < upper_aligned_N; i += 8) {
    x512 = _mm512_loadu_pd(&x[i]);
    y512 = _mm512_loadu_pd(&y[i]);
    result += _mm512_reduce_add_pd(_mm512_mul_pd(x512, y512));
  }

  for (int i = upper_aligned_N; i < N; ++i) {
    result += x[i] * y[i];
  }
#endif

  // TODO compute dot product over all ranks using "MPI_Allreduce" and
  // "data::domain.comm_cart"
  int err = MPI_Allreduce(&result, &result_global, 1, MPI_DOUBLE, MPI_SUM,
                          data::domain.comm_cart);
  assert(err == MPI_SUCCESS);

  return result_global;
}

// computes the 2-norm of x
// x is a vector on length N
double hpc_norm2(Field const& x) { return sqrt(hpc_dot(x, x)); }

// sets entries in a vector to value
// x is a vector on length N
// value is a scalar
void hpc_fill(Field& x, const double value) {
  int N = x.length();

#ifndef AVX_512
  for (int i = 0; i < N; i++) x[i] = value;
#else
  __m128d temp128 = _mm_load_sd(&value);
  __m512d value512 = _mm512_broadcastsd_pd(temp128);
  const int upper_aligned_N = N - N % 8;

  for (int i = 0; i < upper_aligned_N; i += 8) {
    _mm512_storeu_pd(&x[i], value512);
  }

  for (int i = upper_aligned_N; i < N; ++i) {
    x[i] = value;
  }
#endif
}

////////////////////////////////////////////////////////////////////////////////
//  blas level 1 vector-vector operations
////////////////////////////////////////////////////////////////////////////////

// computes y := alpha*x + y
// x and y are vectors on length N
// alpha is a scalar
void hpc_axpy(Field& y, const double alpha, Field const& x) {
  int N = y.length();

#if !defined(AVX_512) || !defined(BLAS)
  for (int i = 0; i < N; i++) y[i] += alpha * x[i];
#elif defined(BLAS)
  cblas_daxpy(N, alpha, x.data(), 1, y.data(), 1);
#else
  __m128d alpha128 = _mm_load_sd(&alpha);
  __m512d alpha512 = _mm512_broadcastsd_pd(alpha128);
  const int upper_aligned_N = N - N % 8;
  __m512d x512, y512;
  for (int i = 0; i < upper_aligned_N; i += 8) {
    x512 = _mm512_loadu_pd(&x[i]);
    y512 = _mm512_loadu_pd(&y[i]);
    _mm512_storeu_pd(&y[i], _mm512_fmadd_pd(alpha512, x512, y512));
  }

  for (int i = upper_aligned_N; i < N; ++i) {
    y[i] += alpha * x[i];
  }
#endif
}

// computes y = x + alpha*(l-r)
// y, x, l and r are vectors of length N
// alpha is a scalar
void hpc_add_scaled_diff(Field& y, Field const& x, const double alpha,
                         Field const& l, Field const& r) {
  int N = y.length();

#ifndef AVX_512
  for (int i = 0; i < N; i++) y[i] = x[i] + alpha * (l[i] - r[i]);
#else
  __m128d alpha128 = _mm_load_sd(&alpha);
  __m512d alpha512 = _mm512_broadcastsd_pd(alpha128);
  const int upper_aligned_N = N - N % 8;
  __m512d x512, l512, r512;
  for (int i = 0; i < upper_aligned_N; i += 8) {
    x512 = _mm512_loadu_pd(&x[i]);
    l512 = _mm512_loadu_pd(&l[i]);
    r512 = _mm512_loadu_pd(&r[i]);
    _mm512_storeu_pd(
        &y[i], _mm512_fmadd_pd(alpha512, _mm512_sub_pd(l512, r512), x512));
  }

  for (int i = upper_aligned_N; i < N; ++i) {
    y[i] = x[i] + alpha * (l[i] - r[i]);
  }
#endif
}

// computes y = alpha*(l-r)
// y, l and r are vectors of length N
// alpha is a scalar
void hpc_scaled_diff(Field& y, const double alpha, Field const& l,
                     Field const& r) {
  int N = y.length();

#ifndef AVX_512
  for (int i = 0; i < N; i++) y[i] = alpha * (l[i] - r[i]);
#else
  __m128d alpha128 = _mm_load_sd(&alpha);
  __m512d alpha512 = _mm512_broadcastsd_pd(alpha128);
  const int upper_aligned_N = N - N % 8;
  __m512d l512, r512;
  for (int i = 0; i < upper_aligned_N; i += 8) {
    l512 = _mm512_loadu_pd(&l[i]);
    r512 = _mm512_loadu_pd(&r[i]);
    _mm512_storeu_pd(&y[i], _mm512_mul_pd(alpha512, _mm512_sub_pd(l512, r512)));
  }

  for (int i = upper_aligned_N; i < N; ++i) {
    y[i] = alpha * (l[i] - r[i]);
  }
#endif
}

// computes y := alpha*x
// alpha is scalar
// y and x are vectors on length n
void hpc_scale(Field& y, const double alpha, Field const& x) {
  int N = y.length();

#ifndef AVX_512
  for (int i = 0; i < N; i++) y[i] = alpha * x[i];
#else
  __m128d alpha128 = _mm_load_sd(&alpha);
  __m512d alpha512 = _mm512_broadcastsd_pd(alpha128);
  const int upper_aligned_N = N - N % 8;
  for (int i = 0; i < upper_aligned_N; i += 8) {
    _mm512_storeu_pd(&y[i], _mm512_mul_pd(alpha512, _mm512_loadu_pd(&x[i])));
  }

  for (int i = upper_aligned_N; i < N; ++i) {
    y[i] = alpha * x[i];
  }
#endif
}

// computes linear combination of two vectors y := alpha*x + beta*z
// alpha and beta are scalar
// y, x and z are vectors on length n
void hpc_lcomb(Field& y, const double alpha, Field const& x, const double beta,
               Field const& z) {
  int N = y.length();

#ifndef AVX_512
  for (int i = 0; i < N; i++) y[i] = alpha * x[i] + beta * z[i];
#else
  __m128d alpha128 = _mm_load_sd(&alpha);
  __m128d beta128 = _mm_load_sd(&beta);
  __m512d alpha512 = _mm512_broadcastsd_pd(alpha128);
  __m512d beta512 = _mm512_broadcastsd_pd(beta128);
  const int upper_aligned_N = N - N % 8;
  __m512d x512, z512;
  for (int i = 0; i < upper_aligned_N; i += 8) {
    x512 = _mm512_loadu_pd(&x[i]);
    z512 = _mm512_loadu_pd(&z[i]);
    _mm512_storeu_pd(
        &y[i], _mm512_fmadd_pd(alpha512, x512, _mm512_mul_pd(beta512, z512)));
  }

  for (int i = upper_aligned_N; i < N; ++i) {
    y[i] = alpha * x[i] + beta * z[i];
  }
#endif
}

// copy one vector into another y := x
// x and y are vectors of length N
void hpc_copy(Field& y, Field const& x) {
  int N = y.length();

#if !defined(AVX_512) || !defined(BLAS)
  for (int i = 0; i < N; i++) y[i] = x[i];
#elif defined(BLAS)
  cblas_dcopy(N, x.data(), 1, y.data(), 1);
#else
  const int upper_aligned_N = N - N % 8;
  for (int i = 0; i < upper_aligned_N; i += 8) {
    // _mm512_movedup_pd
    _mm512_storeu_pd(&y[i], _mm512_loadu_pd(&x[i]));
  }

  for (int i = upper_aligned_N; i < N; ++i) {
    y[i] = x[i];
  }
#endif
}

// conjugate gradient solver
// solve the linear system A*deltax = b for deltax
// the matrix A is implicit in the objective function for the diffusion equation
// the value in deltax constitute the "first guess" at the solution
// deltax(N)
// ON ENTRY contains the initial guess for the solution
// ON EXIT  contains the solution
void hpc_cg(Field& deltax, Field const& x, Field const& b, const int maxiters,
            const double tol, bool& success) {
  // this is the dimension of the linear system that we are to solve
  int nx = data::domain.nx;
  int ny = data::domain.ny;

  if (!cg_initialized) {
    cg_init(nx, ny);
  }

  // epslion value use for matrix-vector approximation
  double eps = 1.e-4;
  double eps_inv = 1. / eps;

  // allocate memory for temporary storage
  hpc_fill(Fx, 0.0);

  // matrix vector multiplication is approximated with
  // A*v = 1/epsilon * ( F(x+epsilon*v) - F(x) )
  //     = 1/epsilon * ( F(x+epsilon*v) - Fxold )
  // we compute Fxold at startup
  // we have to keep x so that we can compute the F(x+exps*v)

  double time = -MPI_Wtime();

  diffusion(x, Fxold);
  time += MPI_Wtime();

  // v = x + epsilon*deltax
  hpc_lcomb(v, 1.0, x, eps, deltax);

  // Fx = F(v)
  diffusion(v, Fx);

  int rank;

  //  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  //  std::cout << rank << ">> " << time << std::endl;

  // r = b - A*x
  // where A*x = (Fx-Fxold)/eps
  hpc_add_scaled_diff(r, b, -eps_inv, Fx, b);

  // p = r
  hpc_copy(p, r);

  // r_old_inner = <r,r>
  double r_old_inner = hpc_dot(r, r), r_new_inner = r_old_inner;

  // check for convergence
  success = false;
  if (sqrt(r_old_inner) < tol) {
    success = true;
    return;
  }

  int iter;
  for (iter = 0; iter < maxiters; iter++) {
    // Ap = A*p
    hpc_lcomb(v, 1.0, x, eps, p);
    diffusion(v, Fx);
    hpc_scaled_diff(Ap, eps_inv, Fx, b);

    // alpha = r_old_inner / p'*Ap
    double alpha = r_old_inner / hpc_dot(p, Ap);

    // deltax += alpha*p
    hpc_axpy(deltax, alpha, p);

    // r -= alpha*Ap
    hpc_axpy(r, -alpha, Ap);

    // find new norm
    r_new_inner = hpc_dot(r, r);

    // test for convergence
    if (sqrt(r_new_inner) < tol) {
      success = true;
      break;
    }

    // p = r + r_new_inner.r_old_inner * p
    hpc_lcomb(p, 1.0, r, r_new_inner / r_old_inner, p);

    r_old_inner = r_new_inner;
  }
  stats::iters_cg += iter + 1;

  if (!success) std::cerr << "ERROR: CG failed to converge" << std::endl;
}

}  // namespace linalg
