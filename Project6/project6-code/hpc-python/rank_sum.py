import numpy as np
from mpi4py import MPI

rank = np.array(MPI.COMM_WORLD.Get_rank())
sum_py = np.array(0)

MPI.COMM_WORLD.Allreduce(rank, sum_py)
sum_c = MPI.COMM_WORLD.allreduce(rank)

if rank == 0:
    print("Python-like MPI: " + str(sum_py))
    print("C-like MPI: " + str(sum_c))