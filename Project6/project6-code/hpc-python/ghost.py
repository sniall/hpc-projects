import numpy as np
from mpi4py import MPI

rank = MPI.COMM_WORLD.Get_rank()
size = MPI.COMM_WORLD.Get_size()

dims = MPI.Compute_dims(size, [0] * 2)
cart = MPI.COMM_WORLD.Create_cart(dims, [False] * 2)

rank_top, rank_bottom = cart.Shift(0, 1)
rank_left, rank_right = cart.Shift(1, 1)

coordinates = cart.coords

print("Rank " + str(rank) + " has coordinates " + str(coordinates) + 
      "\n   and has rank neighbours: north -- " + str(rank_top) + "   east -- " +
      str(rank_right) + "   south -- " + str(rank_bottom) + "   west -- " +
      str(rank_left) + "\n")

# Communicate with neighbouring proceses
rank = np.array(rank)
data_top = np.array(0)
data_bottom = np.array(0)
data_left = np.array(0)
data_right = np.array(0)

cart.Sendrecv(rank, rank_top, 0, data_bottom, rank_bottom, 0)
cart.Sendrecv(rank, rank_right, 0, data_left, rank_left, 0)
cart.Sendrecv(rank, rank_bottom, 0, data_top, rank_top, 0)
cart.Sendrecv(rank, rank_left, 0, data_right, rank_right, 0)

assert rank_top == data_top
assert rank_bottom == data_bottom
assert rank_right == data_right
assert rank_left == data_left