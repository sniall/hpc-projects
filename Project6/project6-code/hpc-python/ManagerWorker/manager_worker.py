from mandelbrot_task import *
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from mpi4py import MPI # MPI_Init and MPI_Finalize automatically called
import numpy as np
import sys
import time
import copy

BENCHMARK = True
RUNS = 25

# some parameters
MANAGER = 0       # rank of manager
TAG_TASK      = 1 # task       message tag
TAG_TASK_DONE = 2 # tasks done message tag
TAG_DONE      = 3 # done       message tag

def manager(comm, tasks):
    """
    The manager.

    Parameters
    ----------
    comm : mpi4py.MPI communicator
        MPI communicator
    tasks : list of objects with a do_task() method perfroming the task
        List of tasks to accomplish

    Returns
    -------
    A list of tasks that are done and an array of how many tasks have been done by the workers.
    """
    
    workers = np.delete(np.arange(comm.size), MANAGER)
    tasks_done_by_worker = [0] * comm.size
    
    remaining_tasks = copy.deepcopy(tasks)
    finished_tasks = []
    
    status = MPI.Status()
    
    """ Initialize workers with tasks """
    for worker in workers:
        if np.size(remaining_tasks) != 0:
            new_task = remaining_tasks[0]
            remaining_tasks = remaining_tasks[1:]

            comm.send(new_task, worker, TAG_TASK)
        else:
            """ Tell worker it is immediately done """
            comm.send(None, worker, TAG_DONE)
    
    """ Receive finished tasks and assign new tasks if available """
    while np.size(finished_tasks) != np.size(tasks):
        task = comm.recv(source=MPI.ANY_SOURCE, tag=TAG_TASK_DONE, status=status)

        worker = status.Get_source()

        finished_tasks.append(task)
        tasks_done_by_worker[worker] += 1

        """ Assign new task or send finished signal """
        if np.size(remaining_tasks) != 0:
            new_task = remaining_tasks[0]
            remaining_tasks = remaining_tasks[1:]

            comm.send(new_task, worker, TAG_TASK)
        else:
            """ Tell worker it is done """
            comm.send(None, worker, TAG_DONE)

    return finished_tasks, tasks_done_by_worker


def worker(comm):
    """
    The worker.

    Parameters
    ----------
    comm : mpi4py.MPI communicator
        MPI communicator
    """
    
    status = MPI.Status()

    while True:
        """ Wait for task from master """
        task = comm.recv(source=MANAGER, tag=MPI.ANY_TAG, status=status)

        if status.Get_tag() == TAG_TASK:
            task.do_work()
            comm.send(task, MANAGER, TAG_TASK_DONE)
        else:
            break


def readcmdline(rank):
    """
    Read command line arguments

    Parameters
    ----------
    rank : int
        Rank of calling MPI process

    Returns
    -------
    nx : int
        number of gridpoints in x-direction
    ny : int
        number of gridpoints in y-direction
    ntasks : int
        number of tasks
    """
    # report usage
    if len(sys.argv) != 4:
        if rank == MANAGER:
            print("Usage: manager_worker nx ny ntasks")
            print("  nx     number of gridpoints in x-direction")
            print("  ny     number of gridpoints in y-direction")
            print("  ntasks number of tasks")
        sys.exit()

    # read nx, ny, ntasks
    nx = int(sys.argv[1])
    if nx < 1:
        sys.exit("nx must be a positive integer")
    ny = int(sys.argv[2])
    if ny < 1:
        sys.exit("ny must be a positive integer")
    ntasks = int(sys.argv[3])
    if ntasks < 1:
        sys.exit("ntasks must be a positive integer")

    return nx, ny, ntasks


if __name__ == "__main__":

    # get COMMON WORLD communicator, size & rank
    comm    = MPI.COMM_WORLD
    size    = comm.Get_size()
    my_rank = comm.Get_rank()

    # report on MPI environment
    if my_rank == MANAGER:
        print(f"MPI initialized with {size:5d} processes")

    # read command line arguments
    nx, ny, ntasks = readcmdline(my_rank)
  
    if BENCHMARK:
        if my_rank == MANAGER:
            file = open(f"mandelbrot_py_bench_{size}.csv", "w")
            file.write("id,ntasks,time\n")
            
        for run in range(RUNS):
            comm.Barrier()
            timespent = -time.perf_counter()
            # trying out
            x_min = -2.
            x_max  = +1.
            y_min  = -1.5
            y_max  = +1.5
            if my_rank == MANAGER:
                M = mandelbrot(x_min, x_max, nx, y_min, y_max, ny, ntasks)
                tasks = M.get_tasks()
                tasks_done, TasksDoneByWorker = manager(comm, tasks)
                m = M.combine_tasks(tasks_done)
            else:
                worker(comm)
                
            timespent += time.perf_counter()
            if my_rank == MANAGER:
                file.write(f"{run},{ntasks},{timespent:.15f}\n")
                
        if my_rank == MANAGER:
            file.close()
            
    else:
        # start timer
        timespent = -time.perf_counter()

        # trying out
        x_min = -2.
        x_max  = +1.
        y_min  = -1.5
        y_max  = +1.5
        if my_rank == MANAGER:
            M = mandelbrot(x_min, x_max, nx, y_min, y_max, ny, ntasks)
            tasks = M.get_tasks()
            tasks_done, TasksDoneByWorker = manager(comm, tasks)
            m = M.combine_tasks(tasks_done)
            plt.imshow(m.T, cmap="gray", extent=[x_min, x_max, y_min, y_max])
            plt.savefig("mandelbrot.png")
        else:
            worker(comm)

        # stop timer
        timespent += time.perf_counter()

        # inform that done
        if my_rank == MANAGER:
            print(f"{my_rank}: Run took {timespent:f} seconds")
            for i in range(size):
                if i == MANAGER:
                    continue
                print(f"Process {i:5d} has done {TasksDoneByWorker[i]:10d} tasks")
            print("Done.")
        else:
            print(f"Worker {comm.rank}: Done.")

