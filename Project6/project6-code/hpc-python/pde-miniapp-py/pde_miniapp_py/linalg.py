"""
Collection of linear algebra operations and CG solver
"""
from mpi4py import MPI
import numpy as np
from . import data
from . import operators


def hpc_dot(x, y, comm):
    """Computes the inner product of x and y"""
    sum_global = np.asarray(0, dtype="d")
    x_ = x.inner.flatten()
    y_ = y.inner.flatten()
    sum = 0.
    for i in range(0, x_.size):
        sum = sum + x_[i] * y_[i]

    # sum = np.dot(x_, y_)
    
    comm.Allreduce([sum, MPI.DOUBLE], [sum_global, MPI.DOUBLE])

    return sum_global


def hpc_norm2(x, comm):
    """Computes the 2-norm of x"""
    norm = hpc_dot(x, x, comm)
    return np.sqrt(norm)


class hpc_cg:
    """Conjugate gradient solver class: solve the linear system A x = b"""

    def __init__(self, domain):
        self._Ap = data.Field(domain)
        self._r = data.Field(domain)
        self._p = data.Field(domain)

        self._xold = data.Field(domain)
        self._v = data.Field(domain)
        self._Fxold = data.Field(domain)
        self._Fx = data.Field(domain)
        self._v = data.Field(domain)
        self._domain = domain

    def solve(self, A, b, x, tol, maxiter):
        """Solve the linear system A x = b"""
        # initialize
        A(x, self._Ap)
        self._r.inner[...] = b.inner[...] - self._Ap.inner[...]
        self._p.inner[...] = self._r.inner[...]
        delta_kp = hpc_dot(self._r, self._r, self._domain.comm_cart)

        # iterate
        converged = False
        for k in range(0, maxiter):
            delta_k = delta_kp
            if delta_k < tol**2:
                converged = True
                break
            A(self._p, self._Ap)
            alpha = delta_k/hpc_dot(self._p, self._Ap, self._domain.comm_cart)
            x.inner[...] += alpha*self._p.inner[...]
            self._r.inner[...] -= alpha*self._Ap.inner[...]
            delta_kp = hpc_dot(self._r, self._r, self._domain.comm_cart)
            self._p.inner[...] = (self._r.inner[...]
                                  + delta_kp/delta_k*self._p.inner[...])

        return converged, k + 1
