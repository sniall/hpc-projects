\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{float}
\usepackage{hyperref}

\input{assignment.sty}
\begin{document}


\setassignment
\setduedate{May 20, 2021, 12pm (midnight)}

\serieheader{High-Performance Computing Lab for CSE}{2021}{Student: Niall Siegenheim}{Discussed with: Maximilian Herde, Christoph Grötzbach}{Solution for Project 6}{}
\newline

% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% --- Exercise 1 ----------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Parallel Space Solution of a nonlinear PDE using MPI [in total 35 points]}
The goal of this assignment was to examine the parallelisation of a PDE solver for the so-called \emph{Fisher's equation} using MPI and C++.

The Fisher's equation is defined as

$$ \frac{\partial s}{\partial t} = D (\frac{\partial^2 s}{\partial x^2} + \frac{\partial^2 s}{\partial y^2}) + Rs(1-s) $$

The implementation uses a finite-difference approach. Each discretization point in the domain establishes a non-linear equation for every time step solved using Newton's method. 
In every iteration of Newton's method, a linear system is solved using a matrix-free conjugate gradient solver.

\subsection{Initialize and finalize MPI [5 Points]}
First of all, we have to initialize MPI using \verb|MPI_Init| to let every rank know the total number of ranks and its own rank using \verb|MPI_Comm_size| and \verb|MPI_Comm_rank|. 
We also have to make sure all ranks properly terminate at the end of the program using \verb|MPI_Finalize|.

\subsection{Create a Cartesian topology [5 Points]}
Similarly as in Project 3, each rank is assigned a rectangular subdomain of the total domain to perform calculations on. 
First, we have to arrange the ranks in a two-dimensional grid. 
The helper function \verb|MPI_Dims_create| takes the number of ranks and dimensions and returns the number of ranks in each dimension. 
This information along with the periodicity in every dimension can then be given on to \verb|MPI_Cart_create| to create a MPI communicator with a two-dimensional Cartesian layout of ranks. 
Every rank can find its neighbours using \verb|MPI_Cart_shift|. 
We shift the topology in the first dimension to find the northern and southern neighbours and then shift again in the second dimension to find the western and eastern neighbours. 
Here, we have to pay attention to the direction we shift in, as otherwise the cardinal directions will be messed up.

Then, using the \verb|discretization| struct, we can divide the domain between the ranks. 
We calculate the offset and size of every subdomain using our information about the location of every rank in the Cartesian topology, the number of ranks in every dimension and the size of the domain. 
If the domain is not evenly divisible between all ranks, the ranks on the eastern and southern border will receive the left-over discretization points. 
In that case, these ranks will have slightly bigger subdomains and therefore also slightly more work to do.

\subsection{Extend the linear algebra functions [5 Points]}
To be able to quickly perform our simulation, the algebraic methods we employ also need to be implemented efficiently on our data type \verb|Field|. 
It can represent both a matrix and a vector and we access the memory it is saved in using square brackets.

We experiment with three different options: Firstly, we can use simple loops to perform dot product, norm, multiplication-addition, scaling, copy and fill operations.
This is the most inefficient implementation, as it does not exploit any hardware optimizations such as caches and vectorised instruction sets.

Secondly, we can use the AVX-512 instruction set. 
This allows us to perform eight algebraic operations on double-precision numbers at the same time at the cost of reducing portability of the code and making it less readable. 
It is only available on newer Intel and AMD processors (less than five years old).

Lastly, Intel offers its Math Kernel Library (MKL), including a version of the BLAS specifically optimised for Intel processors. 
This limits portability to Intel-only processors, but is the fastest readily available implementation.

Therefore, we use BLAS for all operations it is available for and otherwise use AVX-512 instructions.

\subsection{Exchange ghost cells [10 Points]}
The essential part of parallelising the simulation of the PDE is exchanging data between the ranks. Since the update of a discretization point needs informations from its surrounding points, the ranks need to send and receive ghost cells on every time step. 
Ghost cells are those discretization points bordering each rank.

The exchange of this data is implemented in the method \verb|diffusion|. For every neighbour a rank has, it sends a copy of the data on that boundary of his subdomain to that neighbour and expects similarly from the same neighbour. 
We use non-blocking communication to avoid stalling the ranks while waiting for data.
Note that ranks on the border of the global domain do not have neighbours in certain directions.

During communication proceeds in the background, we perform calculations on the inside of every subdomain which are independent of ghost cells. Therefore, we do not waste precious computation time by stalling all ranks while waiting for communication to complete.

After that, we make sure all communication is done and continue to perform calculations using our freshly received data. 

Here, we also optimise all computations to use BLAS and AVX-512, if available. Otherwise, we resort to the naive implementation using loops.

\subsection{Scaling experiments [10 Points]}
All experiments were run on Euler IV nodes equipped with two 18-core Intel Xeon Gold 6150 CPUs each. These have support for the AVX-512 instruction set. Each experiment was repeated 25 times and its median is shown in all graphs. If the level of significance deviates more than five per cent from the median, it is shown in the graphs. We use the C++ library \verb|liblsb|\footnote{\url{https://spcl.inf.ethz.ch/Research/Performance/LibLSB/}} to time our simulation.

We also need to make another adjustment to the original code to generate comparable samples across different grid sizes.

In the original executable, the parameter $ \alpha $ is set dynamically depending on grid size, integration time and total number of time steps. Since we want to make sure our calculations always converge, we want to fix $ \alpha = 1 $. As the grid size is given when benchmarking, we decide to adjust the total integration time dynamically. Thereby we ensure that the total number of time steps is constant and that we therefore perform the same amount of work compared to the original implementation. This makes sure we can compare computation times between different grid sizes while also knowing our simulations will converge.

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/pde-miniapp/strong_128_iters}
		\caption{domain size $ 128 \times 128 $}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/pde-miniapp/strong_256_iters}
		\caption{domain size $ 256 \times 256 $}
	\end{subfigure}

	\vspace{0.5cm}
	
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/pde-miniapp/strong_512_iters}
		\caption{domain size $ 512 \times 512 $}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/pde-miniapp/strong_1024_iters}
		\caption{domain size $ 1024 \times 1024 $}
	\end{subfigure}

	\caption{Strong scaling experiments for PDE solving in C++}
	\label{fig:pde-miniapp-c-strong}
\end{figure}

First of all, we can observe a plateauing effect in strong scaling experiments in figure \ref{fig:pde-miniapp-c-strong}. Performance improves well up until a certain number of ranks, but from then on it plateaus or even drops as seen in the multi-node experiment for the smallest domain size. 
This is due to the performance improvement by performing calculations in parallel being crushed by communication overhead, as the subdomains start to get very small as the number of ranks increase. 
Therefore, this phenomenon be observed earlier on smaller than on bigger domains.

Also, experiments running on all ranks on a single node generally perform better than experiments running on a node per rank. 
Multi-node simulations benefit from more cache and memory available to a single rank. 
However, communication bottlenecks performance as communication between nodes takes a lot longer than between cores, despite being connected through an InfiniBand network. 
This is the case here as the subdomains are not large enough to benefit from more cache and memory.

\begin{figure}[h]
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/pde-miniapp/weak_128_iters}
		\caption{initial domain size $ 128 \times 128 $}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/pde-miniapp/weak_256_iters}
		\caption{initial domain size $ 256 \times 256 $}
	\end{subfigure}

	\caption{Weak scaling experiments for PDE solving in C++}
	\label{fig:pde-miniapp-c-weak}
\end{figure}

Weak scaling experiments in figure \ref{fig:pde-miniapp-c-weak} show a clear step when exceeding 18 ranks and therefore jumping to the second socket on the node when running in single-node mode. Since every rank has its own node anyway in multi-node mode, we cannot observe this behaviour here.

However, multi-node performance seem to be slightly less spurious across the different number of ranks and converges better than single-node performance.

% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% --- Exercise 2 ----------------------------------------------------------- %
% -------------------------------------------------------------------------- %
% -------------------------------------------------------------------------- %

\section{Python for High-Performance Computing (HPC) [in total 50 points]}

\subsection{Sum of ranks: MPI collectives [5 Points]}
Similarly as in project 3, the goal of this exercise is to calculate the sum of all ranks, given that initially every rank only knows its own in variable \verb|rank|.
To do so, we can use the MPI command \verb|Allreduce|. 
It sends the contents of variable \verb|rank| of every rank to all other ranks and by default sums up all of them.

MPI for Python has similar naming conventions as the original implementation for C and offers two interfaces:
Either, you can use the same arguments as in C, calling the method with a non-capitalised name (\verb|Allreduce|).
This requires you to use \verb|NumPy| arrays as buffer and also brings up the issue of passing the output variables as arguments, as there is no differentiation between declaration and definition of variables in Python.
Or, you use the adapted methods for Python objects which also has a more pythonic signature at the cost of slightly reduced communication speed (\verb|allreduce|).

\subsection{Domain decomposition: Create a Cartesian topology [5 Points]}
MPI offers helper functions to create a Cartesian topology of ranks, which can then be used as a communicator. 
MPI for Python has bindings to the corresponding C functions.

Here, we want to create a 2D non-periodic domain of ranks. First of all, we determine the number of ranks in each dimension using the method \verb|MPI.Compute_dims|, which simply takes the number of ranks and the number of dimensions (with possible restrictions) as arguments. 
Then, we use the method \verb|MPI.COMM_WORLD.Create_cart|, which will assign coordinates to each rank given the previously computed dimension sizes and the periodicity of each dimension. 
In this case, we simply use the global communicator \verb|MPI.COMM_WORLD|, i.e. all available ranks, to form the topology.

Now, we can determine each ranks neighbours by calling the method \verb|cart.Shift| on our new Cartesian communicator. Given the dimension and direction of a shift, it will return which rank would be shifted to this rank and which rank this rank would be shifted to if we applied this transformation.
These are the rank's neighbours.
We have to pay attention the ordering of the output neighbour ranks and the direction of shift.

\subsection{Exchange rank with neighbours [5 Points]}
Lastly, we can now exchange data with our neighbouring ranks. 
To do so, we use the \verb|Sendrecv| method on our Cartesian communicator. 
Every time we send data to one of our neighbours, we have to expect receiving data from the opposite neighbour.

In this example, we simply send our own rank to our neighbours and subsequently receive their rank numbers. 
With this information, we can check whether our previous calculations of neighbouring ranks is correct.

\subsection{Parallel space solution of a non-linear PDE using Python and MPI [in total 15 points]}
The goal of this exercise is to implement the same parallelised PDE solver as in task 1, but this time using Python.

\subsubsection{Change linear algebra functions [5 Points]}
To be able to calculate the solution of our PDE, we first have to implement the computation of dot products and norms on our \verb|Field| data type.

Since a \verb|Field| can be interpreted both as a vector and a matrix, we cannot comply with the rigorous mathematical definition of a dot product. 
However, simply multiplying all corresponding elements of the input fields and subsequently adding these together works for our application.

To calculate the dot product, we have to flatten the two input parameters and then either loop manually over all elements or use \verb|dot| method of \verb|NumPy|. 
As \verb|NumPy| internally uses C code, its method is more than a magnitude better performance in iterations per second than the traditional loop.

Then, we can simply define the norm of a \verb|Field| as the square root of the dot product with itself.

\subsubsection{Exchange ghost cells [5 Points]}
We parallelise the computation of the solution of the PDE by dividing the domain and assigning a rank to each subdomain. 
As the \verb|diffusion| operator on a discretization point needs to know the values of all neighbouring discretization points, before every call neighbouring ranks have to communicate the data that is on the boundary of their subdomains.

To do so, every rank performs non-blocking communication with each of its neighbours using \verb|Isend| and \verb|Irecv| in \verb|exchange_startall|. As we use the fast C-like MPI calls, we need to use \verb|NumPy| arrays as buffers.
Then, after having worked on the interior points to minimize looping time, we call \verb|exchange_waitall| and wait for all ranks to finish their communication.

However, we have to be careful that all ranks call \verb|diffusion| the same number of times. 
Otherwise, some ranks will stall and wait for other ranks to reach the implicit barrier of \verb|exchange_waitall| forever. 
Therefore, we have to enforce that all ranks require the same number of Newton and CG iterations. 
We can do this by making the residuals shared across all ranks using \verb|Allgather| on every iteration.

\subsubsection{Scaling experiments [5 Points]}
All experiments were run on Euler IV nodes with two 18-core Intel Xeon Gold 6150 CPUs each. The median of 25 runs for every experiment is plotted. If the level of significance deviates more than five per cent from the median, it is shown in the plots.

\begin{figure}[h]
	\centering
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/hpc-python/strong_128_iters}
		\caption{domain size $ 128 \times 128 $}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/hpc-python/strong_256_iters}
		\caption{domain size $ 256 \times 256 $}
	\end{subfigure}

	\vspace{0.5cm}
	
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/hpc-python/strong_512_iters}
		\caption{domain size $ 512 \times 512 $}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/hpc-python/strong_1024_iters}
		\caption{domain size $ 1024 \times 1024 $}
	\end{subfigure}

	\caption{Strong scaling experiments for PDE solving in Python}
	\label{fig:pde-miniapp-python-strong}
\end{figure}

As we observe in the strong scaling experiments for different domain sizes in figure \ref{fig:pde-miniapp-python-strong}, the simulation scales quite well until at a certain number of ranks the number of conjugate gradient iterations per second plateaus. 
The overhead of communication with more ranks drowns the improvement of performing computations in parallel. 
This point is reached earlier for coarser grids, as less data has to be communicated.

Performance on a single node is generally better than on multiple nodes, especially on smaller grids.
Here, running every rank on a separate node may provide more available memory and cache to each.
However, communication takes longer between nodes than between cores on a the same node.
Especially for small grids, which have little data to keep in memory, this outweighs any multi node benefits.
Additionally, timings are significantly more reliable on single nodes.

Compared to the same simulation in C++, as seen in figure \ref{fig:pde-miniapp-c-strong}, the Python code runs about four to five times slower. This is to be expected, as Python is an interpreted language, while C++ is compiled. Also, Python offers significantly more ``creature comforts" than C++, such as garbage collection.

\begin{figure}[h]
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/hpc-python/weak_128_iters}
		\caption{initial domain size $ 128 \times 128 $}
	\end{subfigure}
	\hfill
	\begin{subfigure}[h]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig/hpc-python/weak_256_iters}
		\caption{initial domain size $ 256 \times 256 $}
	\end{subfigure}

	\caption{Weak scaling experiments for PDE solving in Python}
	\label{fig:pde-miniapp-python-weak}
\end{figure}

For the weak scaling experiments in figure \ref{fig:pde-miniapp-python-weak}, Python manages quite well to keep performance constant after an initial drop, especially on the bigger domain.

\subsection{A self-scheduling example: Parallel Mandelbrot [20 Points]}
The goal of this exercise is to examine the implementation of a manager-worker algorithm for the Mandelbrot set. 
The idea is for a manager rank to assign tasks to worker ranks to keep them busy until the computation is done.

In previous experiments on the Mandelbrot set we found that an equal division of ranks onto the domain will cause great inequality in the amount of work per rank.
Since we cannot pre-compute the amount of work required for every point in the domain, we now want to assign work more dynamically during runtime.
Here, we uniformly divide the real axis of the given domain into a given number of tasks, which should be a lot bigger than the number of available ranks. 
Since every the computation for every point in the domain can be performed independently, this is easily done. 
Then, every worker gets assigned tasks until all tasks are done.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{fig/hpc-python/performance_manager_worker}
	\caption{Manager-Worker performance for Mandelbrot in Python}
	\label{fig:manager-worker-python}
\end{figure}

We implement this by defining rank 0 as the manager rank. It pre-computes the division of the domain into subdomains and tasks. 
Then, it assigns each worker an initial task by sending it as a Python object with a corresponding tag about the contents of the message.
The worker does the computations and once it is finished, sends it back to the manager.
As long as not all tasks have been completed, the manager waits for messages from its workers. 
On receive, the manager either sends a new task to that worker or an empty message with a tag that there are no tasks left to be completed and it may terminate. 
The manager also keeps track of how many tasks every worker completes.
Once all tasks are completed, it puts them back together to provide the final image.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{fig/hpc-python/performance_manager_worker}
	\caption{Manager-Worker performance for Mandelbrot in Python}
	\label{fig:manager-worker-python}
\end{figure}

Performance experiments were once again run on Euler IV nodes. Note that this application needs at least two ranks to run, as otherwise there are no workers available to do work.

As figure \ref{fig:manager-worker-python} shows, performance improvement drops off with an increasing number of ranks. 
This is due to communication overhead between manager and workers, especially since slower Pythonic MPI communication methods are employed. 
Also, there does not seem to be a significant difference between defining 50 or 100 tasks.

In this implementation, the manager does not contribute to the computations and often stalls, waiting for finished tasks from its workers. 
This could be improved by introducing an event system. 
The manager would do computations too, but interrupt once it receives a task from a worker and only continue once it has sent a new task.

\end{document}
