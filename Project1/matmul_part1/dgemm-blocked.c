/* 
    Please include compiler name below (you may also include any other modules you would like to be loaded)

COMPILER= gnu

    Please include All compiler flags and libraries as you want them run. You can simply copy this over from the Makefile's first few lines
 
CC = cc
OPT = -O2
CFLAGS = -Wall -std=gnu99 $(OPT)
MKLROOT = /opt/intel/composer_xe_2013.1.117/mkl
LDLIBS = -lrt -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm

*/

#include <math.h>

const char* dgemm_desc = "Blocked dgemm.";

inline int min(int a, int b) {
  return a < b ? a : b;
}

/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values. */    
void square_dgemm (int n, double* A, double* B, double* C)
{
  const int vec = 8; // AVX-512 intrinsics / cache line => 8 doubles
  int l1 = floor(sqrt(pow(2, 15) / 3. / sizeof(double))); // 32K L1 cache
  l1 -= l1 % vec;
  int l2 = floor(sqrt(pow(2, 20) / 3. / sizeof(double))); // 1M L2 cache
  l2 -= l2 % l1;
  
  int k, j, i, kk, jj, ii, kkk, jjj, iii;
  // L2-blocking
  for (j = 0; j < n; j += l2) {
    int max_jj = min(l2+j, n);
    
    for (k = 0; k < n; k += l2) {
      int max_kk = min(l2+k, n);
    
      for (i = 0; i < n; i += l2) {
        int max_ii = min(l2+i, n);
        
        // L1-blocking
        for (jj = j; jj < max_jj; jj += l1) {
          int max_jjj = min(max_jj, l1+jj);
          
          for (kk = k; kk < max_kk; kk += l1) {
            int max_kkk = min(max_kk, l1+kk);
            
            for (ii = i; ii < max_ii; ii += l1) {
              int max_iii = min(max_ii, l1+ii);
              
              for (jjj = jj; jjj < max_jjj; ++jjj) {
                for (kkk = kk; kkk < max_kkk; ++kkk) {
                  double b = B[kkk + n * jjj];
                  
                  for (iii = ii; iii < max_iii; ++iii) {
                    C[iii + n * jjj] += A[iii + n * kkk] * b;
                  }
                }
              }
              
            }
          }
        }
        
      }
    }
  }
  
  /*
  int N = n;
  n = l1;
  for (j = 0; j < N; j += n) {
    int max_jj = min(n+j, N);
    
    for (k = 0; k < N; k += n) {
      int max_kk = min(n+k, N);
      
      for (i = 0; i < N; i += n) {
        int max_ii = min(n+i, N);
        // Macro-kernel
        for (jj = j; jj < max_jj; ++jj) {
          for (kk = k; kk < max_kk; ++kk) {
            double b = B[kk + jj * N];
            for (ii = i; ii < max_ii; ++ii) {
              C[ii + jj * N] += A[ii + kk * N] * b;
            }
          }
        }
      }
    }
  }
  */
}
