\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\usepackage{amsmath}
\newcommand{\Mod}[1]{\ (\mathrm{mod}\ #1)}
\usepackage{amssymb}
\usepackage[ruled,vlined]{algorithm2e}
\SetKw{KwBy}{by}
\usepackage{caption}
\usepackage{graphicx} 

\input{assignment.sty}
\begin{document}


\setassignment
\setduedate{09.03.2020 (midnight)}

\serieheader{High-Performance Computing Lab for CSE}{2020}{Student: Niall Siegenheim}{Discussed with: Maximilian Herde, Christoph Grötzbach}{Solution for Project 1}{}
\newline

\assignmentpolicy
In this project you will practice memory access optimization, performance-oriented programming, and OpenMP parallelizaton 
on Euler.

\section{Explaining Memory Hierarchies \punkte{30}}
The goal of this exercise is to understand the behaviour of caches. First of all, we should examine the hardware specifications of our two reference systems, an Euler IV compute node and a personal machine.

The Euler IV compute node uses an 18-core Intel(R) Xeon(R) Gold 6150 processor with a base clock of 2.70 GHz. The personal machine uses a 2-core Intel(R) Core(TM) i7-5600U mobile processor with a base clock of 2.60 GHz. The memory specifications of both machines can be seen in figure \ref{fig:mem_specifications}.

\begin{figure}[h]
\begin{center}
	\begin{tabular}{ |l||r|r| }
		\hline
		& Euler IV& Personal\\
		\hline\hline
		Main Memory & 192 GB & 8 GB \\
		\hline
		L3 Cache & 24 MB & 4 MB \\
		\hline
		L2 Cache & 1024 kB & 256 kB \\
		\hline
		L1 Cache & 32 kB & 32 kB \\
		\hline
	\end{tabular}
	\caption{Comparison of memory specifications on Euler and the personal machine.}
	\label{fig:mem_specifications}
\end{center}
\end{figure}

To analyse the different caches we use a memory benchmark. For different array sizes, we iterate over its elements and access them using different strides. We measure the time it takes to do so and renormalise to find out how long a single memory access takes.

\begin{figure}
	\includegraphics[width=0.95\textwidth]{./img/membench/generic_euler.pdf}
	\caption{Memory benchmark on Euler IV compute node.}
	\label{fig:mem_benchmark_euler}
\end{figure}

\begin{figure}
	\includegraphics[width=0.95\textwidth]{./img/membench/generic_personal.pdf}
	\caption{Memory benchmark on personal machine.}
	\label{fig:mem_benchmark_local}
\end{figure}

We start by examining the behaviour of an Euler IV compute node, depicted in figure~\ref{fig:mem_benchmark_euler}. We can identify a horizontal line at about 0.5-0.7 ns, representing the L1 cache. For array sizes up to 32 KB, we can load the whole array into the L1 cache and therefore always have very low read-write times, as observed for $ csize = 128 $ and $ stride = 1 $, exploiting both temporal and spatial locality. For bigger arrays, we only achieve these times if the stride is smaller than a cache line such that we can make use of spatial locality, or by making the stride so big that all of the cache lines of the elements we access fit into the same set in the L1 cache and use temporal locality, as seen for the case of $ csize = 2^{20} $ and $ stride = csize/2 $. By back-tracking, the line the points of $ stride = csize / 8 $ lie along must be the L2 cache with a read-write speed of about 1 ns. At a speed of about 4.5-5 ns we can identify our L3 cache. Lastly, the arrays of size 32 MB and 64 MB do not fit into L3 cache and therefore require main-memory accesses with a high latency, as seen for strides of 64 B to 4 kB.

Examining the plot for the personal machine in figure~\ref{fig:mem_benchmark_local}, it is immediately obvious that this looks very different. However, a closer second look tells another story: The read-write ``mountain" for big array sizes and strides of 64 B to 4 kB still exists, but is trumped by an even bigger one peaking around a stride of 1 M at about 70 ns for 64 MB array size. I assume that due to interference of the caches with other processes running in the background of the personal machine's operating system we cannot use them as efficiently and therefore force a lot of loading from main memory. Especially the L1 cache is still easily visible in the plot as on Euler IV. However, it becomes far more difficult to identify the L2 and L3 caches.

\section{Optimize Square Matrix-Matrix Multiplication  \punkte{70}}
The goal of this exercise is to optimize square matrix-matrix multiplication without actually improving the arithmetic complexity of $ \mathcal{O}(n^3) $, i.e. we exploit knowledge about our hardware to speed up the computations.

\subsection{Single-threaded Optimization}
First, we will try optimizing the algorithm for single-threaded use. We will start with the easiest measures we can take and proceed to more in-depth methods in later steps.

\subsubsection{Compiler Choice}
The easiest way of speeding up code is simply by choosing the correct compiler and setting the corresponding flags. Because we have decided to optimize our code for Euler IV nodes, which use an Intel Xeon Gold 6150 processor, we could change from the general \verb*|gcc| to the Intel-specific \verb*|icc| compiler as well as passing the optimization flag \verb*|-O3|.

However, to correctly analyse the effect of our changes and avoid being fooled by compiler optimizations, we will stick to the standard \verb*|gcc| compiler and \verb*|-O2| flag. However, we pass the \verb*|-funroll-loops| flag to automatically unroll loops and thereby reduce loop overhead. Lastly, we define \verb*|-DAVX512| when compiling to make sure we are using AVX-512 instructions when running on Euler IV. This will be important in a later step.

\subsubsection{Changing Loop Order}
The naive way to implement matrix-matrix multiplication is to loop over every element of the target-matrix $ C $ and then loop over the elements of the input-matrices $ A $ and $ B $, as we as humans interpret this multiplication as summation of elements in rows and columns:

$$ C_{ij} = \sum_{k=1}^{n} A_{ik} B_{kj} \ \ \ \forall (i, j) \in \{1, \dots, N\}^2 $$

However, this loop-order does not exploit cache locality. As we know that our matrices are stored in a column-major fashion, we can formulate pseudo-code \ref{alg:naive} for the naive implementation, where the matrix indices denote memory locations.

\vspace{0.35cm}
\begin{algorithm}[H]
	\caption{Naive Square Matrix-Matrix Multiplication}
	\For{$i \gets 1$ \KwTo $n$ \KwBy $1$}{
		\For{$j \gets 1$ \KwTo $n$ \KwBy $1$}{
			\For{$k \gets 1$ \KwTo $n$ \KwBy $1$}{
				$ C_{i + n j} \gets C_{i + n j} + A_{i + n k} \cdot B_{k + n j} $
			}
		}
	}
\label{alg:naive}
\end{algorithm}
\vspace{0.35cm}

Looking at the loop order, we will notice that throughout the iterations over $ k $ we jump in memory when accessing $ A $, likely leading to cache misses. As we know that the computation itself is independent from the loop order, we can change it to be $ j $-$ k $-$ i $ in the new algorithm \ref{alg:reorder}.

\vspace{0.35cm}
\begin{algorithm}[H]
	\caption{Re-ordered Square Matrix-Matrix Multiplication}
	\For{$j \gets 1$ \KwTo $n$ \KwBy $1$}{
		\For{$k \gets 1$ \KwTo $n$ \KwBy $1$}{
			\For{$i \gets 1$ \KwTo $n$ \KwBy $1$}{
				$ C_{i + n j} \gets C_{i + n j} + A_{i + n k} \cdot B_{k + n j} $
			}
		}
	}
\label{alg:reorder}
\end{algorithm}
\vspace{0.35cm}

Now, we access the memory coherently when iterating over the inner loop variable $ i $ and therefore make use of spatial cache locality. This improves performance on our reference system Euler IV by about 1300 GFlop/s.

\subsubsection{Blocking}
By the properties of matrix-matrix multiplication, we can split both input matrices into smaller submatrices and multiply these correspondingly to get the submatrices of the target matrix:

$$ \mathbf{C}_{ik} = \sum_{k=1}^{r} \mathbf{A}_{ik} \mathbf{B}_{kj} $$

where $ \mathbf{C}_{ik} $, $ \mathbf{A}_{ik} $ and $ \mathbf{B}_{kj} $ are submatrices of size $ \frac{n}{r} \times \frac{n}{r} $ of matrices $ \mathbf{A} $, $ \mathbf{B} $ and $ \mathbf{C} $. In other words, we can perform block matrix-matrix multiplication by treating the blocks the same as we treated real-valued elements in normal matrix-matrix multiplication.

Now, we will extend our implementation to make use of this property to fill our cache completely. As we found out in the memory benchmark, our processor, the Intel Xeon Gold 6150, has a cache line size of 64 B, a L1-cache size of 32 kB and a L2-cache size of 1024 kB. From this information, we can calculate how big our blocks should be. A block should fit both input matrices and the target matrix. We know that our matrices use \verb*|double| values, which take up eight bytes in C on x86, and that our blocks are square. Therefore, we receive the following theoretical formula:

$$ s = \sqrt{\frac{M}{3 \cdot 8}} $$

where $ s $ is the block-stride and $ M $ is the cache size in bytes. However, we adjust the stride of the L1-cache to be divisible by the length of a cache line and the stride of the L2-cache by the stride of a L1-block. We also recognise that we can pull $ B_{k + n j} $ out of the inner-most loop and store it in a variable to load it from a register instead of cache when reading it.

\begin{algorithm}
	\caption{Blocked Square Matrix-Matrix Multiplication}
	$ s_{cache line} = 8 $ \\
	$ s_{L1} \gets \sqrt{2^{15}/24} $; \: $ s_{L1} \gets s_{L1} - s_{L1} \Mod{s_{cache line}} $ \\
	$ s_{L2} \gets \sqrt{2^{20}/24} $; \: $ s_{L2} \gets s_{L2} - s_{L2} \Mod{s_{L1}} $
	
	\vspace{0.2cm}
	\For{$j_{L2} \gets 1$ \KwTo $n$ \KwBy $s_{L2}$}{
		\For{$k_{L2} \gets 1$ \KwTo $n$ \KwBy $s_{L2}$}{
			\For{$i_{L2} \gets 1$ \KwTo $n$ \KwBy $s_{L2}$}{
				\vspace{0.2cm}
				\For{$j_{L1} \gets j_{L2}$ \KwTo $\min(n, j_{L2}+s_{L2})$ \KwBy $s_{L1}$}{
					\For{$k_{L1} \gets k_{L2}$ \KwTo $\min(n, k_{L2}+s_{L2})$ \KwBy $s_{L1}$}{
						\For{$i_{L1} \gets i_{L2}$ \KwTo $\min(n, i_{L2}+s_{L2})$ \KwBy $s_{L1}$}{
							\vspace{0.2cm}
							\For{$j \gets j_{L1}$ \KwTo $\min(n, j_{L2}+s_{L2}, j_{L1}+s_{L1})$ \KwBy $1$}{
								\For{$k \gets k_{L1}$ \KwTo $\min(n, k_{L2}+s_{L2}, k_{L1}+s_{L1})$ \KwBy $1$}{
									$ b \gets B_{k + n j} $ \\
									\For{$i \gets i_{L1}$ \KwTo $\min(n, i_{L2}+s_{L2}, i_{L1}+s_{L1})$ \KwBy $1$}{
										$ C_{i + n j} \gets C_{i + n j} + A_{i + n k} \cdot b $
									}
								}
							}
						}
					}
				}
			}
		}
	}
\end{algorithm}

Addtionally, in our implementation we pull out $ \min $ terms to avoid recalling that function on every iteration. Surprisingly, using a $ \min $ function significantly improves performance compared to using logic operators.

In total, this improves performance by about 1000 GFlop/s upon simply reordering the loops on Euler IV.

\subsubsection{Intrinsics}
As last step of improving performance, we make use of Intel's AVX-512 instruction set extension, which is available on our Euler IV processor. This extension allows us to perform multiply-add (\verb*|fmadd|) operations on eight triplets of \verb*|double| elements at the same time using quasi-assembly methods. This will significantly speed up operations, but will further diminish the readability of our code as the AVX-512 function names are not easily understandable, we have to deal with remainders of the vector operations and lastly limit the portability. Our local machine does not support AVX-512, but only the AVX2 instruction set extension. Therefore, we can only work with four-value vectors in this case and have to adjust our code accordingly.

Now, instead of simply accessing our values using square brackets, we have to use sophisticated \verb*|load| instructions to load eight values of $ A $ and $ C $ at the same time and a \verb*|broadcast| instruction to put the same value of $ B_{k+nj} $ into an eight-value vector. Now, we can use an \verb*|fmadd| instruction to multiply the eight elements of $ A $ and $ B $ with each other and consequently add these to $ C $. Lastly, we have to store our eight-value vector of $ C $ from the special register back into main memory using a \verb*|store| instruction. To use these features, we have to include the header \verb*|immintrin.h| and pass the compiler flag \verb*|-march=skylake-avx512|. 

\paragraph{Dealing with remainders}
Since we are now operating on eight-value vectors, there will be remainders if the size of the matrix is not divisible by eight. We can either naively loop over the remaining elements to calculate their contribution or use so-called masked AVX vector instructions. Due to the nature of vector instructions, you would expect this option to perform a lot better than naive iterations. However, in experiments, we find that there is no performance difference. This is probably due to the fact that the remainder loops only make up a small part of the total number of calculations and the overhead of setting up the masks. Therefore, we will decide to use naive matrix-matrix multiplication for the remainders. Since we have decided to make the size of the L2 blocks divisible by L1 blocks and the size of L1 blocks divisible by cache lines respectively AVX-512 vectors, we do not have to worry about remainders in any of the outer loops.

We therefore end up with algorithm \ref{alg:intrin}.
\begin{algorithm}
	\caption{Intrinsic Blocked Square Matrix-Matrix Multiplication}
	$ s_{cache line} = 8 $ \\
	$ s_{L1} \gets \sqrt{2^{15}/24} $; \: $ s_{L1} \gets s_{L1} - s_{L1} \Mod{s_{cache line}} $ \\
	$ s_{L2} \gets \sqrt{2^{20}/24} $; \: $ s_{L2} \gets s_{L2} - s_{L2} \Mod{s_{L1}} $
	
	\vspace{0.2cm}
	\For{$j_{L2} \gets 1$ \KwTo $n$ \KwBy $s_{L2}$}{
		\For{$k_{L2} \gets 1$ \KwTo $n$ \KwBy $s_{L2}$}{
			\For{$i_{L2} \gets 1$ \KwTo $n$ \KwBy $s_{L2}$}{
				\vspace{0.2cm}
				\For{$j_{L1} \gets j_{L2}$ \KwTo $\min(n, j_{L2}+s_{L2})$ \KwBy $s_{L1}$}{
					\For{$k_{L1} \gets k_{L2}$ \KwTo $\min(n, k_{L2}+s_{L2})$ \KwBy $s_{L1}$}{
						\For{$i_{L1} \gets i_{L2}$ \KwTo $\min(n, i_{L2}+s_{L2})$ \KwBy $s_{L1}$}{
							\vspace{0.2cm}
							\For{$j \gets j_{L1}$ \KwTo $\min(n, j_{L2}+s_{L2}, j_{L1}+s_{L1})$ \KwBy $1$}{
								\For{$k \gets k_{L1}$ \KwTo $\min(n, k_{L2}+s_{L2}, k_{L1}+s_{L1})$ \KwBy $1$}{
									\vspace{0.2cm}
									Load $ B_{k + n j} $ and broadcast into 8-elem. vector $ \vec{b}_{vec}´ $. \\
									
									\For{$i \gets i_{L1} $ \KwTo $\min(n, i_{L2}+s_{L2}, i_{L1}+s_{L1})$ \KwBy $s_{cache line}$}{
										Load $ A_{i + n k}, \dots, A_{i + n k + 7} $ and $ C_{i + n j}, \dots, C_{i + n j + 7} $ into 8-elem vectors $  \vec{a}_{vec} $ and $ \vec{c}_{vec} $. \\
										$ \vec{c}_{vec} \gets \vec{c}_{vec} + \vec{a}_{vec} \cdot \vec{b}_{vec} $ element-wise. \\
										Store $ \vec{c}_{vec} $ into main memory $ C_{i + n j}, \dots, C_{i + n j + 7} $.
									}
									
									\vspace{0.2cm}
									$ b \gets B_{k + n j} $ \\
									\For{$i$ \KwTo $\min(n, i_{L2}+s_{L2}, i_{L1}+s_{L1})$ \KwBy $1$}{
										$ C_{i + n j} \gets C_{i + n j} + A_{i + n k} \cdot b $
									}
								}
							}
						}
					}
				}
			}
		}
	}
\label{alg:intrin}
\end{algorithm}
This leads to another performance boost of about 2000-3000 GFlop/s on Euler IV.

\subsubsection{Results}
Finally, we end up with five different implementations of square-square matrix multiplication: \verb*|dgemm-naive.c|, \verb*|dgemm-reordered.c|, \verb*|dgemm-blocked.c|, \verb*|dgemm-intrin.c| and \verb*|dgemm-blas.c|.

As seen in figure~\ref{fig:part1_benchmark_euler} and figure~\ref{fig:part1_benchmark_local}, every further step of optimization we take, our performance improves. In total, we are able to increase it about five to six fold from a completely naive to a fairly optimized version when compiling with the same flags. However, the standard Intel MKL implementation is about a factor of ten ahead. Presumably, its code was written in assembly and heavily optimized for the processor we are using.

Interestingly, all of our implementations show dips at matrix sizes of the power of two. These dips occur at multiples of 256 and are therefore probably a result of cache trashing, i.e. when data we access is aligned in a way that they constantly evict each other from cache when accessing them. Also, the intrinsic implementation runs slightly quicker on the personal machine compared to Euler IV.

\begin{figure}
	\includegraphics[width=0.95\textwidth]{img/matmul_part1/timing_euler.pdf}
	\caption{Benchmark of different single-threaded optimized matrix-matrix multiplication algorithms on Euler IV.}
	\label{fig:part1_benchmark_euler}
\end{figure}

\begin{figure}
	\includegraphics[width=0.95\textwidth]{img/matmul_part1/timing_local.pdf}
	\caption{Benchmark of different single-threaded optimized matrix-matrix multiplication algorithms on a local machine.}
	\label{fig:part1_benchmark_local}
\end{figure}

\subsection{Multi-threaded Optimization}
The goal of this part is to take the code from part 1 and optimize it to exploit multiple threads using OpenMP.

As we know from part 1, in block matrix-matrix multiplication each block in the target matrix can be computed independently. Therefore, the idea is to assign a thread to each $ i-j $ L1 block in the target matrix $ C $. OpenMP lets us do this relatively easily using a preprocessor directive in front of our to be parallelised region: The beginning \verb|pragma omp| is obligatory. Then, \verb|parallel| tells OpenMP that it can expect to parallelise tasks in the following scope. Lastly, \verb|for collapse(2)| informs OpenMP that the next construction consisting of two for-loops should be run parallelised.

To make this pragma work, a few adjustments have to be made to the code from part 1. First and foremost, the loop order for L1-blocks has to be changed from $ j-k-i $ to $ k-j-i $, as otherwise the two loops to be parallelised are not immediately consecutive. However, this does not influence relevant caching behaviour. Additionally, to avoid having to declare variables as private, all of their declarations needed for computation are moved inside the \verb*|parallel| region.

\begin{algorithm}
	\caption{Intrinsic Blocked Multi-Thread Square Matrix-Matrix Multiplication}
	$ s_{cache line} = 8 $ \\
	$ s_{L1} \gets \sqrt{2^{15}/24} $; \: $ s_{L1} \gets s_{L1} - s_{L1} \Mod{s_{cache line}} $ \\
	$ s_{L2} \gets \sqrt{2^{20}/24} $; \: $ s_{L2} \gets s_{L2} - s_{L2} \Mod{s_{L1}} $
	
	\vspace{0.2cm}
	\For{$j_{L2} \gets 1$ \KwTo $n$ \KwBy $s_{L2}$}{
		\For{$k_{L2} \gets 1$ \KwTo $n$ \KwBy $s_{L2}$}{
			\For{$i_{L2} \gets 1$ \KwTo $n$ \KwBy $s_{L2}$}{
				\vspace{0.2cm}
				\For{$k_{L1} \gets k_{L2}$ \KwTo $\min(n, k_{L2}+s_{L2})$ \KwBy $s_{L1}$}{
					\vspace{0.2cm}
					Advise compiler to parallelise to following two loops. \\
					\For{$j_{L1} \gets j_{L2}$ \KwTo $\min(n, j_{L2}+s_{L2})$ \KwBy $s_{L1}$}{
						\For{$i_{L1} \gets i_{L2}$ \KwTo $\min(n, i_{L2}+s_{L2})$ \KwBy $s_{L1}$}{
							\vspace{0.2cm}
							\For{$j \gets j_{L1}$ \KwTo $\min(n, j_{L2}+s_{L2}, j_{L1}+s_{L1})$ \KwBy $1$}{
								\For{$k \gets k_{L1}$ \KwTo $\min(n, k_{L2}+s_{L2}, k_{L1}+s_{L1})$ \KwBy $1$}{
									\vspace{0.2cm}
									Load $ B_{k + n j} $ and broadcast into 8-elem. vector $ b_{vec} $. \\
									
									\For{$i \gets i_{L1} $ \KwTo $\min(n, i_{L2}+s_{L2}, i_{L1}+s_{L1})$ \KwBy $s_{cache line}$}{
										Load $ A_{i + n k}, \dots, A_{i + n k + 7} $ and $ C_{i + n j}, \dots, C_{i + n j + 7} $ into 8-elem vectors $ a_{vec} $ and $ c_{vec} $. \\
										$ c_{vec} \gets c_{vec} + a_{vec} \cdot b_{vec} $ element-wise. \\
										Store $ c_{vec} $ into main memory $ C_{i + n j}, \dots, C_{i + n j + 7} $.
									}
									
									\vspace{0.2cm}
									$ b \gets B_{k + n j} $ \\
									\For{$i$ \KwTo $\min(n, i_{L2}+s_{L2}, i_{L1}+s_{L1})$ \KwBy $1$}{
										$ C_{i + n j} \gets C_{i + n j} + A_{i + n k} \cdot b $
									}
								}
							}
						}
					}
				}
			}
		}
	}
\end{algorithm}

\subsubsection{Results}
To benchmark our code, we compile and run \verb*|dgemm-naive.c|, \verb*|dgemm-blocked-single.c| (aka \verb*|dgemm-intrin.c| from part 1, single-threaded), \verb*|dgemm-blocked.c| (multi-threaded) and \verb*|dgemm-blas.c|. We also make sure to request enough cores when running this job on Euler IV.

Executing our parallelised code on ten threads, this immediately yields eight fold speed-up as seen in figure \ref{fig:part2_benchmark_euler} and \ref{fig:part2_benchmark_local}. Our performance fluctuates significantly more than our single-threaded implementation. This is due to how evenly the blocks can be distributed among the threads. Despite this nearly linear speed-up, the standard multi-threaded Intel MKL implementation is still at least five times quicker.

\begin{figure}
	\includegraphics[width=0.95\textwidth]{img/matmul_part2/timing_euler.pdf}
	\caption{Benchmark of single- and multi-threaded optimized matrix-matrix multiplication algorithms on Euler IV.}
	\label{fig:part2_benchmark_euler}
\end{figure}

\begin{figure}
	\includegraphics[width=0.95\textwidth]{img/matmul_part2/timing_local.pdf}
	\caption{Benchmark of single- and multi-threaded optimized matrix-matrix multiplication algorithms on the local machine.}
	\label{fig:part2_benchmark_local}
\end{figure}

Additionally, the L2-blocks could be parallelised too. However, in experiments it shows that at least in the case of our matrix sizes the overhead is too big of spawning threads for these blocks and the computations slow down significantly.

Lastly, the hardware limitations of the personal machine become very obvious: Because there are only two physical cores, parallelising multiplication hardly gives any benefit due to the overhead of initialising the threads.

\end{document}
