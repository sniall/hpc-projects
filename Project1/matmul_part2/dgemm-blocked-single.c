/* 
    Please include compiler name below (you may also include any other modules you would like to be loaded)

COMPILER= gnu

    Please include All compiler flags and libraries as you want them run. You can simply copy this over from the Makefile's first few lines
 
CC = cc
OPT = -O3
CFLAGS = -Wall -std=gnu99 $(OPT)
MKLROOT = /opt/intel/composer_xe_2013.1.117/mkl
LDLIBS = -lrt -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm

*/

#include <math.h>
#include <immintrin.h>

const char* dgemm_desc = "Intrinsic dgemm.";

inline int min(int a, int b) {
  return a < b ? a : b;
}

/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values. */    
void square_dgemm (int n, double* A, double* B, double* C)
{
  const int vec = 8; // AVX-512 intrinsics / cache line => 8 doubles
  int l1 = floor(sqrt(pow(2, 15) / 3. / sizeof(double))); // 32K L1 cache
  l1 -= l1 % vec;
  int l2 = floor(sqrt(pow(2, 20) / 3. / sizeof(double))); // 1M L2 cache
  l2 -= l2 % l1;
  
#ifdef AVX512
  __m128d bkj;
  __m512d aik, bkj_bc, cij;
#else
  __m128d bkj;
  __m256d aik, bkj_bc, cij;
#endif

  int k, j, i, kk, jj, ii, kkk, jjj, iii;
  
  // L2-blocking
  for (j = 0; j < n; j += l2) {
    int max_jj = min(l2+j, n);
    
    for (k = 0; k < n; k += l2) {
      int max_kk = min(l2+k, n);
    
      for (i = 0; i < n; i += l2) {
        int max_ii = min(l2+i, n);
        
        // L1-blocking
        for (jj = j; jj < max_jj; jj += l1) {
          int max_jjj = min(max_jj, l1+jj);
          
          for (kk = k; kk < max_kk; kk += l1) {
            int max_kkk = min(max_kk, l1+kk);
            
            for (ii = i; ii < max_ii; ii += l1) {
              int max_iii = min(max_ii, l1+ii);
              
              for (jjj = jj; jjj < max_jjj; jjj++) {
                for (kkk = kk; kkk < max_kkk; kkk++) {
                  bkj = _mm_loadu_pd(&B[kkk + n * jjj]); // load two elements
                  
#ifdef AVX512
                  bkj_bc = _mm512_broadcastsd_pd(bkj); // broadcast low elem into 8-elem vector
                  
                  // First: Intrinsics
                  for (iii = ii; iii < max_iii-vec+1; iii += vec) {
                    aik = _mm512_loadu_pd(&A[iii + n * kkk]); // load 8-elem vector
                    cij = _mm512_loadu_pd(&C[iii + n * jjj]); // load 8-elem vector
                    
                    cij = _mm512_fmadd_pd(aik, bkj_bc, cij); // perform multiply-add
                    
                    _mm512_storeu_pd(&C[iii + n * jjj], cij); // store 8-elem vector
                  }
#else
                  bkj_bc = _mm256_broadcastsd_pd(bkj); // broadcast low elem into 8-elem vector
                  // First: Intrinsics
                  for (iii = ii; iii < max_iii-vec+1; iii += 4) {
                    aik = _mm256_loadu_pd(&A[iii + n * kkk]); // load 8-elem vector
                    cij = _mm256_loadu_pd(&C[iii + n * jjj]); // load 8-elem vector
                    
                    cij = _mm256_fmadd_pd(aik, bkj_bc, cij); // perform multiply-add
                    
                    _mm256_storeu_pd(&C[iii + n * jjj], cij); // store 8-elem vector
                  }
#endif
                  
                  // Remaining parts: Naive
                  double b = B[kkk + n * jjj];
                  for (; iii < max_iii; iii++) {
                    C[iii + n * jjj] += A[iii + n * kkk] * b;
                  }
                  
                  /*
                  // Remaining parts: Masked intrinsics
                  unsigned int bound, no_set_bits, mask;
                  __mmask8 m;
                  
                  for (; iii < max_iii; iii += vec) {
                    no_set_bits = min(max_iii - iii, vec);
                    mask = 0;
                    
                    // Set bits that should be masked
                    for (iter = 0; iter < no_set_bits; iter++) {
                      mask |= 1 << iter; 
                    }
                    m = _cvtu32_mask8(mask);
                    
                    aik = _mm512_mask_loadu_pd(aik, m, &A[iii + n * kkk]); // load 8-elem vector
                    cij = _mm512_mask_loadu_pd(cij, m, &C[iii + n * jjj]); // load 8-elem vector
                    cij = _mm512_mask_fmadd_pd(aik, m, bkj_bc, cij); // perform multiply-add
                    
                    _mm512_mask_storeu_pd(&C[iii + n * jjj], m, cij); // store 8-elem vector
                  }
                  */
                }
              }
              
            }
          }
        }
        
      }
    }
  }
}
