set title "NxN matrix-matrix-multiplication on 2-Core Intel(R) Core(TM) i7-5600U @ 2.60GHz"
set xlabel "Matrix size (N)"
set ylabel "Performance (GFlop/s)"
set grid
set logscale y 10
# set key outside;
set key right bottom;

set terminal postscript color "Helvetica" 14
set output "timing.ps"

# set terminal png color "Helvetica" 14
# set output "timing.png"

# plot "timing.data" using 2:4 title "square_dgemm" with linespoints


# For performance comparisons

plot "timing_basic_dgemm.data"   using 2:4 title "Naive dgemm" with linespoints, \
     "timing_blocked_dgemm_single.data" using 2:4 title "Blocked intrinsic single-thread dgemm" with linespoints, \
     "timing_blocked_dgemm.data" using 2:4 title "Blocked intrinsic multi-thread dgemm" with linespoints, \
     "timing_blas_dgemm.data"   using 2:4 title "MKL blas dgemm" with linespoints
